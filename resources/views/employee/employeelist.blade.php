@php
foreach($employeelist as $key =>$value){
   $employee_id= Crypt::encrypt($value->employee_id);
   if($value->image == null){
     $image= 'man.jpg';
   }else{
     $image = $value->image;
   } 
@endphp
   <tr>
    
   <td><image src="{{asset('assets/images/public/'.$image)}}" height="50%" width="50%"></image></td>
   <td>{{$value->employee_name}}</td>
   <td>{{$value->email}}</td>
   <td>{{$value->employee_phone_no}}</td>
   <td> @if(request()->has('trashed'))
                <a href="{{ route('qualification.restore', $employee_id) }}" class="btn btn-success">Restore</a>
            @else
                <form method="POST" action="{{ route('employee.destroy', $employee_id) }}">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <button type="submit" class="btn btn-danger delete" title='Delete'><i class="fa fa-close color-danger"></i></button>
                </form>
            @endif<span><a href="{{url('dashboard/employee/update',$employee_id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a></td>
</tr>
@php    
}
@endphp
<td colspan="5" align="center">
        {{$employeelist->links('pagination::bootstrap-4')}}
       </td>