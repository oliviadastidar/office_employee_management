<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Demo',
            'email' => 'demo@email.com',
            'password' => bcrypt('password'),
            'usertype' => 'admin',
            'status' => 'active'
        ]);
    }
}
