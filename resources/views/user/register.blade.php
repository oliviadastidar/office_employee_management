@include('template.header')

    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                                
                                <a class="text-center" href="{{url('/login')}}"> <h4>Assignment</h4></a>
                                @if (count($errors) > 0)

                                   <div class="alert alert-danger">

                                    <ul>

                                      @foreach ($errors->all() as $error)

                                         <li>{{ $error }}</li>

                                      @endforeach

                                      </ul>

                                    </div>

                                @endif

                                @if ($message = Session::get('success'))

                                <div class="alert alert-success">

                                      <p>{{ $message }}</p>
                                      <p class="mt-5 login-form__footer"><a href="{{url('/login')}}" class="text-primary">Sign In </a> now</p>

                                </div>

                                @else
                                <form class="mt-5 mb-5 login-input" action="{{url('/userregistration')}}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="firstname" placeholder="FirstName" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control"  name="lastname" placeholder="LastName" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name= "retypepassword" placeholder="RetypePassword" required>
                                    </div>
                                    <button class="btn login-form__btn submit w-100">Sign up</button>
                                </form>
                                    <p class="mt-5 login-form__footer">Have account <a href="{{url('/login')}}" class="text-primary">Sign In </a> now</p>
                                    </p>
                                @endif    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('template.footer')