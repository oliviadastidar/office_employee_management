<?php

namespace Database\Seeders;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeCoordinator;
use Illuminate\Database\Seeder;

class EmployeeCoordinatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          $dept_Array = ['f_1','hr_1'];
        //$dept_Array = ['pl_1','pm_1','qa_1','ra_1','sa_1','ui_1','wd_1'];
        foreach ($dept_Array as $key=>$value) {
            # code...
      
            $getdata = Department::getdepartmentdetail($value);
      
            foreach ($getdata as $keyno => $keyvalue) {
                # code...
                $get_employee = Employee::getemployeelist($keyvalue['department_id']);
                foreach ($get_employee as $keyemployee => $valueemployee) {
                    # code...
                    $array[] =["employee_id"=>$valueemployee['employee_id']];
                }
            }
        }
        EmployeeCoordinator::Insert($array);
    }
}
