@extends('template.master')
@section('content')
    
    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{url('dashboard/memberinfo')}}">Memberlist</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0);">User Detail</a></li>
            </ol>
        </div>
    </div>
 
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                    <!--personal details  -->
                        <div><h3 align="center">User Personal Details</h3><br /> 
                            <span id="edit" style="position:absolute;right:10px;top:25px;" value="false"><i class="fa fa-pencil color-muted m-r-5"></i> </span></div>
                              
                              @if(isset($employeepersonaldetails))
                                @foreach($employeepersonaldetails as $personaldetail)
                                
                                <form class="form-valide" action="{{url('/dashboard/employeelist/edit')}}" id="personaldetail"  method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                                @csrf
                                    <input type="hidden" value="{{ isset($personaldetail->employee_id)? Crypt::encrypt($personaldetail->employee_id) : ' ' }}" name="employee_id" id="emp_per_id">
                                          
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="Department-name">Employee Image<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                               
                                            @php
                                            if(($personaldetail->image =='')||($personaldetail->image ==NULL)){ @endphp
                                                <img id="preview-image-before-upload" src="#"
                                                alt="preview image" style="max-height: 250px;">
                                                
                                            @php }else{ @endphp
                                                <img id="preview-image-before-upload" src="{{asset('assets/images/public/'.$personaldetail->image)}}"
                                                alt="preview image" style="max-height: 250px;">
                                                        
                                            @php   }@endphp
                                                

                                                <input id="imageactivate" type="file" name="image" placeholder="Choose image" value={{$personaldetail->image}}>
                                                @if ($errors->has('image'))
                                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
             
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Name<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_name" name="employee_name" value="{{ isset($personaldetail->employee_name)? $personaldetail->employee_name : ' ' }}" placeholder="Enter a Employee name.." readonly>
                                                 @if ($errors->has('employee_name'))
                                                    <span class="text-danger">{{ $errors->first('employee_name') }}</span>
                                                @endif
                                            </div>
                                        </div> 
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Gender<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6" id="gen">
                                                <input type="radio" id="gendermale" name="gender" value="male" {{($personaldetail->gender == 'male') ? 'checked':"";}}>
                                                 <label for="html">Male</label>
                                                <input type="radio" id="genderfemale" name="gender" value="female"{{($personaldetail->gender == "female") ? 'checked' :"";}}>
                                                <label for="html">Female</label>
                                            </div>
                                             @if ($errors->has('gender'))
                                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                                            @endif
                                        </div> 
                                         
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Phone<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_phone_no" name="employee_phone_no" value="{{ isset($personaldetail->employee_phone_no)? $personaldetail->employee_phone_no : ' ' }}" placeholder="Enter a Employee Phone Number.." readonly>
                                                @if ($errors->has('employee_phone_no'))
                                                    <span class="text-danger">{{ $errors->first('employee_phone_no') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Email<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="email" name="email" value="{{ isset($personaldetail->email)? $personaldetail->email : ' ' }}" placeholder="Enter a Employee Email.." readonly>
                                                 @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">Address <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control" id="address" name="address" rows="5" placeholder="What would you like to see?" readonly>{{ isset($personaldetail->address)?$personaldetail->address: ' ' }}</textarea>
                                            </div>
                                             @if ($errors->has('address'))
                                                <span class="text-danger">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                       
                                        <div class="form-group row" id="personaldetailaction">
                                            <div class="col-lg-8 ml-auto">
                                                <button id="personaldetailsave" class="btn btn-success btn-rounded"  type="submit" >Save</button>
                                                <button type="reset" class="btn btn-primary">Clear</button>
                                            </div>
                                        </div>
                                    </form>
                                @endforeach
                              @endif
                            </div>
                        </div>
                    </div>
                </div> 
                @php
                $emp_id = ' ';
                $payslip = ' '; 
                $experience_letter = ' '; 
                $release_letter = ' '; 
                $designation = ' ';
                $ex_in_old_co = ' ';
                $ex_in_new_co = ' ';
                $qualifications= ' ';
                $technicalskills = ' ';
                @endphp
                @if(isset($employeeprofessionaldetails))
                              
                                        @foreach($employeeprofessionaldetails as $professionaldetail)
                                        @php
                                            $emp_id = isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : ' ';
                                            $payslip =  isset($professionaldetail->payslip)? $professionaldetail->payslip : ' '; 
                                            $experience_letter =  isset($professionaldetail->experience_letter)? $professionaldetail->experience_letter : ' '; 
                                            $release_letter =  isset($professionaldetail->release_letter)? $professionaldetail->release_letter : ' '; 
                                            $designation = isset($professionaldetail->designation)? $professionaldetail->designation : ' ';
                                            $ex_in_old_co = isset($professionaldetail->experience_in_previous_company)? $professionaldetail->experience_in_previous_company : ' ';
                                             $ex_in_new_co = isset($professionaldetail->experience_in_our_company)? $professionaldetail->experience_in_our_company : ' ';
                                             $qualifications=isset($professionaldetail->qualification)? $professionaldetail->qualification : ' ';
                                             $technicalskills = isset($professionaldetail->technical_skills)? $professionaldetail->technical_skills : ' ';
                                             @endphp
                                        @endforeach
                                    @endif
                                    
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                            <!--proffessional details -->
                            
                                <div><h3 align="center">User Professional Details</h3><br /> 
                                    <span id="editprofessional" style="position:absolute;right:10px;top:25px;"><i class="fa fa-pencil color-muted m-r-5"></i> </span></div>
                                            
                                            <form class="form-valide" id="proffesionaldataform" action="{{url('/dashboard/employeelist/professionaldataedit')}}" method="post">
                                            @csrf
                                                <input type="hidden" value="{{ $emp_id }}" name="employee_id" id="employee_id">
                                      
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="Department-name">Department Name<span class="text-danger">*</span>
                                                    </label>
                                                <div class="col-lg-6">
                                                    <select id="department_name" name="department_name" class="form-control form-control-lg" readonly>
                                                        <option value="">none</option> 
                                                        @foreach($department_list as $list) 
                                                        {{$list->department_name}}
                                                            @php if($list->department_name == $employeedepartmentname[0]->department_name){
                                                                $checked='selected';
                                                            }else{
                                                                $checked = '';
                                                            }
                                                            @endphp
                                                        <option value="{{$list->department_id}}" {{$checked}}>{{$list->department_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Designation<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_destination" name="employee_designation" value="{{ $designation }}" placeholder="Enter a Employee Phone Number.." readonly>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">Experience in previous company <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_experience_in_previous_company" name="employee_experience_in_previous_company" value="{{ $ex_in_old_co }}" placeholder="Enter a Employee Phone Number.." readonly>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">Experience in our company <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_experience_in_our_company" name="employee_experience_in_our_company" value="{{ $ex_in_new_co }}" placeholder="Enter a Employee Phone Number.." readonly>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">qualification <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                 
                                                   <select id="employee_qualification" name="employee_qualification" class="form-control form-control-lg" readonly>
                                                        <option value="">none</option> 
                                                       
                                                        @foreach($qualification as $qualifylist) 
                                                            @php if($qualifylist->id == $qualifications){
                                                                $checked='selected';
                                                            }else{
                                                                $checked = '';
                                                            }
                                                            @endphp
                                                        <option value="{{$qualifylist->id}}" {{$checked}}>{{$qualifylist->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>    
                                            </div>
                                        
                                        <div class="form-group row" >
                                            <label class="col-lg-4 col-form-label" for="Description">technical skill <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_technical_skill" name="employee_technical_skill" value="{{ $technicalskills }}" placeholder="Enter a Employee Phone Number.." readonly>
                                            </div>
                                        </div>
                                        

                                        <div id="uploaddetails">
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="Description">release letter <span class="text-danger">*</span>
                                                </label>
                                                <div class="col-lg-6">
                                                    <input type ="hidden" id="letterrelease" value="{{$release_letter}}">
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalshow" onclick="checkletter();">view</button>
                                                </div>
                                            </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">expererience letter <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                 
                                                <input type ="hidden" id="experienceletter" value="{{$experience_letter}}">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalshow" onclick="checkexpletter();">view</button>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">payslip <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                
                                                <input type ="hidden" id="payslip" value="{{$payslip}}">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalshow" onclick="checkpayslip();">view</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="proffesionaldetailaction" class="form-group row">
                                        <div class="col-lg-8 ml-auto">
                                            <button id="proffesionaldatasave" class="btn btn-success btn-rounded"  type="submit">Save</button>
                                            <button type="clear" class="btn btn-primary">Clear</button>
                                        </div>
                                    </div>
                                </form>
                                
                        </div>
                    </div>
                </div>
            </div> 
        </div>
            <!-- #/ container -->
    </div>

    <div class="modal fade" id="modalshow">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" onclick="clear();"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="message"></div>
                </div>
            </div>
        </div>
    </div>
        <!--**********************************
            Content body end

            ***********************************-->  
            
    <script>
                
        $("#imageactivate").hide();
        $('#personaldetailaction').hide(); 
        $('#proffesionaldetailaction').hide();
        $('#gendermale').attr('disabled', true);
        $('#genderfemale').attr('disabled', true);

        //edit button work
        $("#edit").click(function(){
            $("#employee_name").attr("readonly", false); //personaldetailaction
            $("#employee_phone_no").attr("readonly", false);
            $("#email").attr("readonly", false);
            $("#address").attr("readonly", false); 
            $('#personaldetailaction').show(); 
            $('#edit').hide(); 
            $("#imageactivate").show();
            $('#gendermale').attr('disabled', false);
            $('#genderfemale').attr('disabled', false);
        });

        //editprofessional button work
        $("#editprofessional").click(function(){
            $("#department_name").attr("readonly", false); //personaldetailaction
            $("#employee_destination").attr("readonly", false);
            $("#employee_experience_in_previous_company").attr("readonly", false);
            $("#employee_experience_in_our_company").attr("readonly", false);
            $("#employee_qualification").attr("readonly", false);
            $("#employee_technical_skill").attr("readonly", false);
            $('#uploaddetails').hide();
            $('#proffesionaldetailaction').show();
        });
                    
        function checkletter(){
            var value = $('#letterrelease').val();
            if ($('#releaseletter').length){
                /* it exists */
                $("#releaseletter").remove();
            }
            if(value == ' '){
                $("#message").html("no data available.Please provide only pdf file only");
                var form = '<form action="{{ url("/dashboard/employeelist/fileupload") }}" method="POST" enctype="multipart/form-data">@csrf<input type="hidden" value="{{ isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : " " }}" name="employee_id" id="employee_id"><input type="hidden" name="choice" value="value"><div class="row"><div class="col-md-6"><div id="pdfinput"><input id="releaseletter" type="file" name="releaseletter" placeholder="Choose image" value="value"><button type="submit" class="btn btn-success">Upload</button></div></div></div></form>';
                $("#message").after(form);
            }else{
                $("#message").html("<embed src='{{url('uploads/professional/documents/')}}+value' id='releaselettershow' type='application/pdf' width='100%' height='600px' />");
                var form = ' <form action="{{ url("/dashboard/employeelist/fileupload") }}" method="POST" enctype="multipart/form-data">@csrf<input type="hidden" value="{{ isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : " " }}" name="employee_id" id="employee_id"><input type="hidden" name="choice" value="value"><div class="row"><div class="col-md-6"><div id="pdfinput"><input id="releaseletter" type="file" name="releaseletter" placeholder="Choose image" value="value"><button type="submit" class="btn btn-success">Upload</button></div></div></div></form>';
                $("#message").after(form);
            }
        }

    function checkexpletter(){
        var value = $('#experienceletter').val();
        if ($('#releaseletter').length){
            /* it exists */
            $("#releaseletter").remove();
        }
        if(value == ' '){
            $("#message").html("no data available.Please provide only pdf file only");
            var form = ' <form action="{{ url("/dashboard/employeelist/fileupload") }}" method="POST" enctype="multipart/form-data">@csrf<input type="hidden" value="{{ isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : " " }}" name="employee_id" id="employee_id"><input type="hidden" name="choice" value="value"><div class="row"><div class="col-md-6"><div id="pdfinput"><input id="releaseletter" type="file" name="releaseletter" placeholder="Choose image" value="value"></div><div class="col-md-6"><button type="submit" class="btn btn-success">Upload</button></div></div></div></form>';
            $("#message").after(form);
        }else{
            $("#message").html("<embed src='{{url('uploads/professional/documents/')}}+value' id='releaselettershow' type='application/pdf' width='100%' height='600px' />");
            var form = ' <form action="{{ url("/dashboard/employeelist/fileupload") }}" method="POST" enctype="multipart/form-data">@csrf<input type="hidden" value="{{ isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : " " }}" name="employee_id" id="employee_id"><input type="hidden" name="choice" value="value"><div class="row"><div class="col-md-6"><div id="pdfinput"><input id="releaseletter" type="file" name="releaseletter" placeholder="Choose image" value="value"><div class="col-md-6"><button type="submit" class="btn btn-success">Upload</button></div></div></div></form>';
            $("#message").after(form);
        }
    }

    function checkpayslip(){
        var value = $('#payslip').val();
        if ($('#releaseletter').length){
            /* it exists */
            $("#releaseletter").remove();
        }
        if(value == ' '){
            $("#message").html("no data available.Please provide only pdf file only");
            $("#message").html("<embed src='{{url('uploads/professional/documents/')}}+value+' id='releaselettershow' type='application/pdf' width='100%' height='600px' />"); 
            var form = ' <form action="{{ url("/dashboard/employeelist/fileupload") }}" method="POST" enctype="multipart/form-data">@csrf<input type="hidden" value="{{ isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : " " }}" name="employee_id" id="employee_id"><input type="hidden" name="choice" value="value"><div class="row"><div class="col-md-6" id="pdfinput"><input id="releaseletter" type="file" name="releaseletter" placeholder="Choose pdf" value="value"><button type="submit" class="btn btn-success">Upload</button></div></div></div></form>';
            $("#message").after(form);
        }else{
            $("#message").html("<embed src='{{url('uploads/professional/documents/')}}+value+' id='releaselettershow' type='application/pdf' width='100%' height='600px' />"); 
            var form = ' <form action="{{ url("/dashboard/employeelist/fileupload") }}" method="POST" enctype="multipart/form-data">@csrf<input type="hidden" value="{{ isset($professionaldetail->employee_id)? Crypt::encrypt($professionaldetail->employee_id) : " " }}" name="employee_id" id="employee_id"><input type="hidden" name="choice" value="value"><div class="row"><div class="col-md-6" id="pdfinput"><input id="releaseletter" type="file" name="releaseletter" placeholder="Choose pdf" value="value"><button type="submit" class="btn btn-success">Upload</button></div></div></form>';
            $("#message").after(form);
        }
    }
                
    function clear(){
        $("#modalshow").html("");
    }
      
    $(document).ready(function (e) {
        $('#imageactivate').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => { 
                                        $('#preview-image-before-upload').attr('src', e.target.result); 
                                    }
            reader.readAsDataURL(this.files[0]); 
        });
    });
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script>
        var emp_name = '';
        var emp_ph_no = '';
        var emp_email = '';
        var emp_add = '';
        var dept_name = '';
        var emp_desig = '';
        var empexp_old = '';
        var empexpnew = '';
        var emp_quali ='';
        var emptech_skill ='';
        var e_id_per = $("#emp_per_id").val();
        if(e_id_per == ''){
            emp_name = false;
            emp_ph_no = false;
            emp_email = false;
            emp_add = false;
            dept_name = false;
            emp_desig = false;
            empexp_old = false;
            empexpnew = false; 
            emp_quali = false;
            emptech_skill = false;
        }else{
            emp_name = true;
            emp_ph_no = true;
            emp_email = true;
            emp_add = true;
            dept_name = true;
            emp_desig = true;
            empexp_old = true;
            empexpnew = true; 
            emp_quali = true;
            emptech_skill = true;
        }
    
    $(document).on('keyup', '#department_name', function(){
        departmentnamevalidate();
    });
    
    $(document).on('keyup', '#employee_destination', function(){
        employeedestinationvalidate();
    });

    $(document).on('keyup', '#employee_experience_in_previous_company', function(){
        employeeexperienceoldvalidate();
    });

    $(document).on('keyup', '#employee_experience_in_our_company', function(){
        employeeexperiencenewvalidate();
    });

    $(document).on('keyup', '#employee_qualification', function(){
        employeequalivalidate();
    });

    $(document).on('keyup', '#employee_technical_skill', function(){
        employeetechskillvalidate();
    });

    $(document).on('keyup', '#address', function(){
         addressvalidate();
    });

    $(document).on('keyup', '#employee_phone_no', function(){
        employeephonenumber();
    });
    
    $(document).on('keyup', '#email', function(){
         emailvalidate();
    });

    $('#personaldetail').submit(function (e) {
        if((dept_name == true)&&(emp_desig == true)&&(empexp_old == true)&&(empexpnew == true)&&(emp_quali == true)&&(emptech_skill == true)){
            $('#proffesionaldatasave').removeAttr('disabled'); 
        }else{
            $('#proffesionaldatasave').prop('disabled', true);
        }
    });

    $('#proffesionaldataform').submit(function (e) {
        if((dept_name == true)&&(emp_desig == true)&&(empexp_old == true)&&(empexpnew == true)&&(emp_quali == true)&&(emptech_skill == true)){      
            return true;
       }else{
            e.preventDefault();
       }
    });

    function departmentnamevalidate(){
        var deptname = $("#department_name").val();     
        if(deptname == ''){
            response('department_name','departmentnameerror',"Department Name Cannot Be Blank");
            dept_name = false;
        }else{
            dept_name = true;
        } 
        return dept_name;
    } 

    function employeedestinationvalidate(){
        var empdesign = $("#employee_destination").val();
        var regEx =  /^[a-zA-Z\s]*$/;
        if(empdesign == ''){
            response('employee_destination','employdesignationeerror',"Employee Designation Cannot Be Blank");
            emp_desig = false;
        }else{
            if(regEx.test(empdesign)){
                $("#employdesignationeerror").remove();
                emp_desig = true;
            }else{
                response('employee_destination','employdesignationeerror',"Please enter letters,number and space only.");
                emp_desig = false;
            }
        } 
        return emp_desig;
    }

     function employeeexperienceoldvalidate(){
        var empexpold = $("#employee_experience_in_previous_company").val();
        var regEx =  /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/;
        if(empexpold == ''){
              response('employee_experience_in_previous_company','empexpolderror',"Employee Name Cannot Be Blank");
              empexp_old = false;
        }else{
            if(regEx.test(empexpold)){
                $("#empexpolderror").remove();
                empexp_old = true;
            }else{
                response('employee_experience_in_previous_company','empexpolderror',"Please enter letters and space only.");
                empexp_old = false;
            }
        } 
        return empexp_old;
    }

    function employeeexperiencenewvalidate(){
        var empexp_new = $("#employee_experience_in_our_company").val();
        var regEx =  /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/;
        if(empexp_new == ''){
            response('employee_experience_in_our_company','empexpolderror',"Employee Name Cannot Be Blank");
            empexpnew = false;
        }else{
            if(regEx.test(empexp_new)){
                $("#empexpolderror").remove();
                empexpnew = true;
            }else{
                response('employee_experience_in_our_company','empexpolderror',"Please enter letters and space only.");
                empexpnew = false;
            }
        } 
        return empexpnew;
    }

    function employeequalivalidate(){
        var empquali = $("#employee_qualification").val();
        var regEx =  /^[A-Za-z]+[A-Za-z ]*$/;
        if(empquali == ''){
            response('employee_qualification','employeequalierror',"Employee Name Cannot Be Blank");
            emp_quali = false;
        }else{
            if(regEx.test(empquali)){
                $("#employeenameerror").remove();
                emp_quali = true;
            }
        } 
        return emp_quali;
    } 

     function employeetechskillvalidate(){
        var emptechskill = $("#employee_technical_skill").val();
        if(emptechskill == ''){
              response('employee_technical_skill','employeetechskillerror',"Employee Technical Skill Cannot Be Blank");
              emptech_skill= false;
        }else{
                $("#employeetechskillerror").remove();
                emptech_skill = true;
            
        } 
        return emptech_skill;
    }

    function employeenamevalidate(){
        var empname = $("#employee_name").val();
        var regEx =  /^[a-zA-Z\s]*$/;
        if(empname == ''){
            response('employee_name','employeenameerror',"Employee Name Cannot Be Blank");
            emp_name = false;
        }else{
            if(regEx.test(empname)){
                $("#employeenameerror").remove();
                emp_name = true;
            }else{
                response('employee_name','employeenameerror',"Please enter letters and space only.");
                dept_name = false;
            }
        } 
        return emp_name;
    }

    function addressvalidate(){
        var address = $("#address").val();    
        if(address == ''){
            response('Address','addresserror',"Address Cannot Be Blank");
            emp_add = false;
        }else{
            $("#addresserror").remove();
            emp_add = true;
        } 
        return emp_add;
    }
    
    function employeephonenumber(){
        var ph_numbers = $("#employee_phone_no").val();
        var numbers = /^[0-9]+$/;
        if(ph_numbers == ''){
            response('Phone_numbers','phonenumbererror',"Phone Numbers Name Cannot Be Blank Or 0");
            emp_ph_no = false;
        }else{
            if(isNaN(Number(ph_numbers))){
                response('Phone_numbers','phonenumbererror',"Please input numeric characters only");
                emp_ph_no = false;  
            }else{
                $("#phonenumbererror").remove();
                emp_ph_no = true;
            }
        }
        return emp_ph_no; 
    }

    function emailvalidate(){
        var email = $("#email").val();
        if(email == ' '){
            response('Email','emailerror',"Email Cannot Be Blank");
            emp_email = false;  
        }else{
            let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (email.match(regexEmail)) {    
                emp_email= true; 
            } else {
                emp_email= false; 
            }
        }
        return emp_email;
    }

    $(document).on("change", "#imageactivate", function() {
        imagecheck();
    });
 
    function imagecheck(){
        var img='';
        var image = $('#imageactivate').val();
        if(image == ''){
            response('imageactivate','imageerror',"Insert Image");
            img = false;
        }else{
            var myImg = this.files[0];
            var myImgType = myImg["type"];
            var validImgTypes = ["image/gif", "image/jpeg", "image/png"];
            
            if ($.inArray(myImgType, validImgTypes) < 0) {
                response('imageactivate','imageerror',"Not An Image");
                img=false;
            }else{
                img = true;
            }
        }
        return img;
    }

    function response(id,errorid,msg){
        if ($('#'+errorid).length){
            /* it exists */
            $("#"+errorid).remove();
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }else{
            /* it doesn't exist */
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }
    }
    
    function radiocheck(){
        var checkRadio = document.querySelector('input[name="gender"]:checked'); 
        var radiocheck ='';
        if (checkRadio == null) {
            response('gen', 'gendererror', 'No one selected');
            radiocheck = false;
        }else{
            radiocheck = true;
        }   
        return radiocheck;  
    }
   
    if((emp_name == true)&&(emp_ph_no == true)&&(emp_email == true)&&(emp_add == true)){ 
        $('#personaldetailsave').removeAttr('disabled');   
    }else{
        $('#personaldetailsave').prop('disabled', true);
    }

    if((dept_name == true)&&(emp_desig == true)&&(empexp_old == true)&&(empexpnew == true)&&(emp_quali == true)&&(emptech_skill == true)){
        $('#proffesionaldatasave').removeAttr('disabled'); 
    }else{
        $('#proffesionaldatasave').prop('disabled', true);
    }
    
    //proffesionaldatasave
</script>
@stop

