 <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">

                    <li>
                        <a class="has-arrow" href="{{url('dashboard')}}" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Dashboard</span>
                        </a>
                        
                    </li>
                    <li>
                        <a class="has-arrow" href="{{url('dashboard/department')}}" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Department</span>
                        </a>
                        
                    </li>
                    <li>
                        <a class="has-arrow" href="{{url('dashboard/employee')}}" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Employee</span>
                        </a>
                        
                    </li>
                    
                    <li>
                        <a class="has-arrow" href="{{url('dashboard/employeelist')}}" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Employeelist</span>
                        </a>
                        
                    </li>
                    <li>
                        <a class="has-arrow" href="{{url('dashboard/qualification')}}" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">Qualification</span>
                        </a>
                        
                        <li>
                        <a class="has-arrow" href="{{url('dashboard/user/logout')}}" aria-expanded="false">
                            <i class="icon-speedometer menu-icon"></i><span class="nav-text">logout</span>
                        </a>
                        
                    </li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->