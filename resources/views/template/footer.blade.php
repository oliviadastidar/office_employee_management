   <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{ asset('/assets/plugins/common/common.min.js')}}"></script>
    <script src="{{ asset('/assets/js/custom.min.js')}}"></script>
    <script src="{{ asset('/assets/js/settings.js') }}"></script>
    <script src="{{ asset('/assets/js/gleek.js') }}"></script>
    <script src="{{ asset('/assets/js/styleSwitcher.js') }}"></script>

    <!-- Chartjs -->
    <script src="{{ asset('/assets/plugins/chart.js/Chart.bundle.min.js') }}"></script>
    <!-- Circle progress -->
    <script src="{{ asset('/assets/plugins/circle-progress/circle-progress.min.js') }}"></script>
    <!-- Datamap -->
    <script src="{{ asset('/assets/plugins/d3v3/index.js') }}"></script>
    <script src="{{ asset('/assets/plugins/topojson/topojson.min.js') }}"></script>
    <!-- <script src="{{ asset('/assets/plugins/datamaps/datamaps.world.min.js') }}"></script> -->
    <!-- Morrisjs -->
    <script src="{{ asset('/assets/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/morris/morris.min.js') }}"></script>
    <!-- Pignose Calender -->
    <script src="{{ asset('/assets/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/pg-calendar/js/pignose.calendar.min.js') }}"></script>
    <!-- ChartistJS -->
    <script src="{{ asset('/assets/plugins/chartist/js/chartist.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js') }}"></script>

    


    <script src="{{ asset('/assets/js/dashboard/dashboard-1.js') }}"></script>
     <!-- <script src="{{ asset('/assets/plugins/tables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tables/js/datatable/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/assets/plugins/tables/js/datatable-init/datatable-basic.min.js') }}"></script> -->

    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Delete Product</h5>
                   <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
              </div>
             <input type="text" id="product_id" hidden>

             <div class="modal-body">Do you want to delete it?</div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onClick="remove();">Yes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

</body>

</html>
<script>
     
    // imgInp.onchange = evt => {
    //                                   const [file] = imgInp.files
    //                                   if (file) {
    //                                      logo_image.src = URL.createObjectURL(file)
    //                                   }
    //                                  }
</script>