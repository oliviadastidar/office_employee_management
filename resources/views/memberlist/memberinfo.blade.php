

                                         @if(isset($memberlist))
    @foreach($memberlist as $key=>$data)
      @php $employee_id= Crypt::encrypt($data->employee_id); @endphp
                 <!--Encrypt ID and store as $prodID-->
        <tr>
            <td>{{$data->employee_id}}</td>
            <td>{{$data->employee_name}}</td>
            <td>{{$data->employee_dept_id}}</td>
            <td>{{$data->employee_phone_no}}</td>
            <td>{{$data->email}}</td>
            <td>{{$data->address}}</td>
            <td><span><a href="{{url('dashboard/membersinfo/update',$employee_id)}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a><a href="#" data-toggle="tooltip" data-placement="top" title="Close"><i class="fa fa-close color-danger"></i></a></span></td>
        </tr>
    @endforeach
    <tr>
       <td colspan="3" align="center">
        {!! $memberlist->links() !!}
       </td>
@endif
                                       