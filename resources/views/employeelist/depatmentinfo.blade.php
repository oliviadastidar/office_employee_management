@php
if(isset($data['department_detail']['image'])){
    $img = $data['department_detail']['image'];
}else{
    $img= 'project-manager.png';
}
@endphp
<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
            <th width="10%"><image src="{{asset('assets/images/public/'.$img)}}" height="60" width="60"></th>
            <th width="10%"><h3>{{$data['department_detail']['department_name']}}</h3></th>
            <th width="70%"><h3>{{$data['department_detail']['description']}}</h3></th>
            <th colspan="2" align="center"width="10%"><h3>{{$data['department_detail']['total_members']}}</h3></th>
        </tr>
        </thead>
        <thead>
            <tr>
                <th width="10%">Image</th>
            <th width="10%" class="sorting" data-sorting_type="asc" data-column_name="employee_name-{{$data['department_detail']['department_id']}}" style="cursor: pointer"><h4>Employee Name</h4><span id="department_name"><span class="glyphicon glyphicon-triangle-top"></span></span></th>
            <th width="10%" class="sorting" data-sorting_type="asc" data-column_name="designation-{{$data['department_detail']['department_id']}}" style="cursor: pointer"><h4>Designation</h4><span id="department_name"><span class="glyphicon glyphicon-triangle-top"></span></span></th>

            <th width="70%" class="sorting" data-sorting_type="asc" data-column_name="email-{{$data['department_detail']['department_id']}}" style="cursor: pointer"><h4>Email</h4><span id="total_members"><span class="glyphicon glyphicon-triangle-top"></span></span></th>
            <th width="10%">Action</th>
                
            </tr>    
        </thead>
        <tbody id="{{$data['department_detail']['department_id']}}">
            @include('employeelist.employeelist');
        </tbody>  
    </table>
</div>  
<input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="employee_name" />
    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
          