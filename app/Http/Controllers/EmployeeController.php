<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Session;
use App\Models\Employee;
use App\Models\Department;
use App\Models\EmployeeProfessionalDetail;
use App\Models\Qualification;
use Illuminate\Support\Str;
use App\Http\Middleware\isAdmin;
use App\Models\EmployeeCoordinator;
use App\Http\Middleware\isaccess;

class EmployeeController extends Controller
{
    public function __construct(){
      $this->middleware(isaccess::class);
    }
        
    public function index(Request $request){
       $employeelist = [];
       if ($request->has('trashed')) {
            $employeelist = Employee::getthrashed();
        } else {
           $employeelist = Employee::getmemberlist();
        }
       
      
       return view('employee.index',compact('employeelist')); 
    }

    public function proffesionaldatastore(Request $request){
        $id = Crypt::decrypt($request->employee_id);
         $validator = validator()->make(request()->all(), [
                        'department_name'=>'required',
                        'employee_designation' => 'required|regex:/^[a-zA-Z\s]*$/',
                         'employee_experience_in_previous_company' => 'required|regex:/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/',
                        'employee_experience_in_our_company' => 'required|regex:/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/',
                        'employee_qualification' => 'required|integer',
                        'employee_technical_skill'=> 'required', 
        ]);
       
        if ($validator->fails())
        {
          return back()->withErrors($validator)->withInput();
        }else{
            $array = array(
                        'designation'=>$request->employee_designation,
                        'experience_in_previous_company'=>$request->employee_experience_in_previous_company,
                        'experience_in_our_company'=>$request->employee_experience_in_our_company,
                        'qualification'=>$request->employee_qualification,
                        'technical_skills'=>$request->employee_technical_skill,
            );
            
            $update = EmployeeProfessionalDetail::scopeUpdateemployee($id, $array);
        }
           if($update){
            return back()->with('success','data updated successfully');
           }else{
            return back()->with('fail','data updation not successful');
           }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $validator = validator()->make(request()->all(), [
                        'employee_name' => 'required|regex:/^[\pL\s\-]+$/u|unique:employee',
                         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                        'gender' => 'required',
                        'department_name' =>'required',
                         'employee_phone_no'=> 'required|integer', 
                         'email' => 'required|email|unique:employee',
        ]);
        
        $uuid = Str::uuid()->toString();
        
        if ($validator->fails())
        {
          return back()->withErrors($validator)->withInput();
        }else{ 
             $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('assets/images/public/'), $imageName);
            $array = [
                        'employee_id'=> $uuid,
                        'employee_name' =>$request->employee_name,
                        'image' =>$imageName,
                        'gender'=>$request->gender,
                        'employee_phone_no'=>$request->employee_phone_no,
                        'email'=>$request->email,
            ];
            $arr = ['employee_id'=>"'".$uuid."'",'departement_id'=>"'".$request->department_name."'",'supervisor'=>null];

           $update = Employee::scopeAddemployee($array);
            
            $add = EmployeeCoordinator::add($arr);
            
           $addproffessionaldata = EmployeeProfessionalDetail::addemloyee($uuid);
           
           if(($update == 1)&&($add ==1)&&($addproffessionaldata ==1)){
               return redirect('/dashboard/employee');
            }else{
                  return back()->with('fail','data updation not successful');
           }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //user choice
        if($id != ''){
          $department_type = $id;
        }else{
          $department_type = 'Technical';
        }
        
        //array list depend on the department
        if($department_type)
        {
            if($department_type == "Technical"){
                $dept_Array = ['pm_1','pl_1','wd_1','qa_1','ra_1','sa_1','ui_1'];
            
            }else{
                 $dept_Array = ['f_1','hr_1'];
                
            }
        }
       
        $department_detail = Department::scopeGetalldata($dept_Array); //department detail list
        
        foreach($department_detail as $key=>$value){
           
           $dept_employee = Employee::scopeEmployeelist($value[0]->department_id); //employee information

           $response_array[] = array('department_detail'=>$value[0],'list'=>$dept_employee,);
        }
        $final_response = $response_array;
        
        return view('employeelist.employeeinfo',compact('department_type','final_response'));
    }

    public function upload(Request $request){

        $request->validate([
            'releaseletter' => 'required|mimes:pdf,xlx,csv',
        ]);
        
        $fileName = $request->input('choice').time().'.'.$request->releaseletter->extension();  
        $id = Crypt::decrypt($request->employee_id);
         
        $request->releaseletter->move(public_path('uploads/professional/documents'), $fileName);
        $array = array(
                        $request->input('choice')=> $fileName,
            );
           $update = EmployeeProfessionalDetail::scopeUpdateemployee($id,$array);
        //    die('i am just tired and exhausted');
           if($update){
            return back()->with('success','You have successfully upload file.');
           }else{
            return back()->with('fail','data updation not successful');
           }
        
    }

    public function create(){
        $department_list = Department::getlist();
        return view('employee.add_edit',compact('department_list'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $id = Crypt::decrypt($request->employee_id);
        $department_list = Department::getlist();
       
        $qualification =Qualification::getlist();
        $employeepersonaldetails = Employee::getPersonalDetails($id);
        $employeeprofessionaldetails = EmployeeProfessionalDetail::getemployeeprofessionaldetails($id);
        
        $employeedepartmentid = Employee::getdepartmentid($id);
       
        $employeedepartmentname = Department::getdepartmentname($employeedepartmentid);
    
        return view('employeelist.add_edit',compact('department_list','qualification','employeepersonaldetails','employeeprofessionaldetails','employeedepartmentname'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($employee_id)
    {
        //
        $id = Crypt::decrypt($employee_id);
        $cleardata = Employee::destroy($id);
         
        return redirect()->back();
    }

    public function getfilterdata(Request $request){
        
        if($request->ajax())
        {
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
            $query = $request->input('search');
            $query = str_replace(" ", "%", $query);
            $employeelist =Employee::filteremployeedata($sort_by,$sort_type,$query);
           
        } else{
            $employeelist = "data not available";
        }  

         return view('employee.employeelist',compact('employeelist')); 
        
       }


    public function restore($employee_id)
    {
       $id = Crypt::decrypt($employee_id);
      $restore = Employee::restoreid($id);
  
        return redirect()->back();
    }

    public function restoreAll()
    {
        $restoreall = Employee::restoreall();
  
        return redirect()->back();
    }
}
