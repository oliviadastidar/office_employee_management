@extends('template.master')
@section('content')
    
    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{url('dashboard/qualification')}}">Qualification</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0);">Qualification Detail</a></li>
            </ol>
        </div>
    </div>
    @php
        $id ='';
        $name = '';
        $detail = '';
    @endphp

        @if(isset($qualificationdetail))
        @foreach($qualificationdetail as $detail)
       
        @php
        
        $id = isset($detail->id)? Crypt::encrypt($detail->id) : ' ';
        $name =isset($detail->name)? $detail->name : ' ';
        $detail = isset($detail->detail)? $detail->detail : 'm ';
        @endphp
        @endforeach
        @endif  
        
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <!--personal details  -->
                            <div><h3 align="center">Qualification Details</h3><br /> 
                                <span id="edit" style="position:absolute;right:10px;top:25px;" value="false"><i class="fa fa-pencil color-muted m-r-5"></i> </span></div>
               
                                <form class="form-valide" action="{{url('/dashboard/qualification/edit')}}" id="personaldetail"  method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                                    @csrf
                                    <input type="hidden" value="{{$id}}" name="qualification_id" id="emp_per_id">
             
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="Department-name">Name<span class="text-danger">*</span>
                                            </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" id="name" name="name" value="{{$name}}" placeholder="Enter a degree name..">
                                                 @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                                @endif
                                        </div>
                                    </div> 
 
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="Department-name">Detail
                                            </label>
                                        <div class="col-lg-6">
                                            <textarea type="text" class="form-control" id="detail" name="detail" value="{{ $detail }}" placeholder="Enter a Employee Phone Number..">{{$detail}}</textarea>
                                            
                                            @if ($errors->has('detail'))
                                            <span class="text-danger">{{ $errors->first('detail') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row" id="detailaction">
                                        <div class="col-lg-8 ml-auto">
                                            <button id="personaldetailsave" class="btn btn-success btn-rounded"  type="submit" >Save</button>
                                            <button type="reset" class="btn btn-primary">Clear</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- #/ container -->
        </div>

    <script >
    $(document).ready(function (e) {

        $('#imageactivate').change(function(){      
            let reader = new FileReader();
            reader.onload = (e) => { 
                                        $('#preview-image-before-upload').attr('src', e.target.result); 
                                    }
            reader.readAsDataURL(this.files[0]); 
        });
    });
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script>
        var name = '';
        
        $(document).on('keyup', '#name', function(){
            namevalidate();
        });

        function namevalidate(){
            var name_id = $("#name").val();
            if(name_id == ''){
                response('name','nameerror',"Name Cannot Be Blank");
                name = false;
            }else{
                $("#nameerror").remove();
                name = true;   
            } 
            return name;
        }
       
    $(document).on("change", "#imageactivate", function() {
        imagecheck();
    });
 
    function imagecheck(){
        var img='';
        var image = $('#imageactivate').val();
        if(image == ''){
            response('imageactivate','imageerror',"Insert Image");
            img = false;
        }else{
            var myImg = this.files[0];
            var myImgType = myImg["type"];
            var validImgTypes = ["image/gif", "image/jpeg", "image/png"];
            
            if ($.inArray(myImgType, validImgTypes) < 0) {
                response('imageactivate','imageerror',"Not An Image");
                img=false;
            }else{
                img = true;
            }
        }
        return img;
    }

    function response(id,errorid,msg){
        if ($('#'+errorid).length){
        /* it exists */
            $("#"+errorid).remove();
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }else{
        /* it doesn't exist */
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }
    }
 
   
</script>
@stop
