
@php
foreach($data['list'] as $key =>$value){
   $employee_id= Crypt::encrypt($value->employee_id); 

if(isset($value->image)){
    $img = $value->image;
}else{
    $img= 'man.jpg';
}
@endphp
   <tr>
   <td><image src="{{asset('assets/images/public/'.$img)}}" height="50%" width="50%"></image></td>
   <td>{{$value->employee_name}}</td>
   <td>{{$value->designation}}</td>
   <td>{{$value->email}}</td>
   <td><span><a href="{{url('dashboard/employeelist/update',$employee_id)}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a></td>
</tr>
@php    
}
@endphp
<td colspan="5" align="center">
        {{$data['list']->links('pagination::bootstrap-4')}}
       </td>