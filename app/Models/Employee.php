<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'employee';
    
    protected $fillable = [
                            'employee_id',
                            'employee_name',
                            'employee_dept_id',
                            'employee_phone_no',
                            'email',
                            'address', 
                          ];

    protected $gaurded = ['employee_id'];

    public static function getmemberlist(){
        $response = Employee::orderby('employee_id','asc')->paginate(10);
        return $response;
    }

    public static function scopeAddemployee($array){
        $response = Employee::insert($array);
        return $response;
    }
    public static function getiddetail($id){
        
        $detail = Employee::select("*")
                        ->where("employee_id", $id)
                        ->get();
        
        return $detail;
    }

    

    public static function updateemployee($uuid,$id,$data){
    
        $array = [
                    'employee_id' => $uuid,
                    'employee_name'=> $data['employee_name'],
                    'employee_dept_id'=> $data['employee_dept_id'],
                    'employee_phone_no' => $data['employee_phone_no'],
                    'email' => $data['email'],
                    'address' => $data['address'],
        ];
        
        $response = Employee::updateOrInsert(
        ['employee_id' => ''],
        $array
    );
        return $response;

    }

    public static function getdata($id){
        //dd($id);
        $response = Employee::select('employee_id','employee_dept_id')->where("employee_dept_id",[$id])->get();
        //dd($response);
        return $response;
    }

    public static function getemployeelist($department_id){
        $response = Employee::select('employee_id')->where("employee_dept_id",[$department_id])->get();
        //dd($response);
        return $response;
    }

    public static function scopeEmployeelist($dept){
      
         $response = Employee::select('employeecoordinator.employee_id','employee.employee_name','employee.email','employeeprofessionaldetail.designation')->join('employeeprofessionaldetail', 'employee.employee_id', '=', 'employeeprofessionaldetail.employee_id')->join('employeecoordinator', 'employee.employee_id', '=', 'employeecoordinator.employee_id')->where('employeecoordinator.departement_id','=',$dept)->paginate(5);
        
          return $response;
    }

    public static function getPersonalDetails($id){
        $response = Employee::where('employee_id',$id)->get();
        return $response;
    }
    
    public static function getdepartmentid($id){
        $response = EmployeeCoordinator::select('departement_id')->where('employee_id',$id)->get();
        return $response;
    }

    public static function filterdata($sort_by,$sort_type,$query,$dept){
        
        $response = Employee::select('employeecoordinator.employee_id','employee.image','employee.employee_name','employee.email','employeeprofessionaldetail.designation')->join('employeeprofessionaldetail', 'employee.employee_id', '=', 'employeeprofessionaldetail.employee_id')->join('employeecoordinator', 'employee.employee_id', '=', 'employeecoordinator.employee_id')->where('employeecoordinator.departement_id','=',$dept)->orWhere('employee.employee_name', 'like', '%'.$query.'%')->orWhere('employee.email', 'like', '%'.$query.'%')->orWhere('employeeprofessionaldetail.designation', 'like', '%'.$query.'%')->orderBy($sort_by, $sort_type)->paginate(5);
      
          return $response;
    }

    public static function filteremployeedata($sort_by,$sort_type,$query){
        
        $response = Employee::Where('employee_name', 'like', '%'.$query.'%')
                            ->orWhere('email', 'like', '%'.$query.'%')
                            ->orWhere('employee_phone_no', 'like', '%'.$query.'%')
                            ->orderBy($sort_by, $sort_type)
                            ->paginate(10);
        
          return $response;
    }

    public static function scopeUpdateemployee($id,$data){
        // dd($data->employee_id);
       
       $response = Employee::where('employee_id', $id)  // find your user by their email
        ->update($data); 
        
        return $response;
    }
      public static function getthrashed(){
        $response = Employee::onlyTrashed()
                ->get();

                return $response;
    }
      public static function destroy($id){
        $response =Employee::where('employee_id',$id)->delete();
        return $response;
    }
    public static function restoreid($id){
        $restore = Employee::withTrashed()->where('employee_id',$id)->restore();
        return $restore;
    }
    public static function restoreall(){
        $restore = Employee::onlyTrashed()->restore();
         return $restore;
    }
}
