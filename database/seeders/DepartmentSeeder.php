<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
            $data = 
            [
                [
                    [ 
                        'department_id'=>'hr_1',
                        'department_name'=>'Human Resource Team',
                        'total_members' => 10,
                        'description'=>"Human resources (HR) is the division of a business that is charged with finding, screening, recruiting, and training job applicants, as well as administering employee-benefit programs.",
                    ],
                    [
                        'department_id'=>'f_1',
                        'department_name'=>'Finance Team',
                        'total_members' => 10,
                        'description'=>"The finance department ensures the adequate and timely provision of funds for the business's operations.",
                    ],
                    [
                        'department_id'=>'ra_1',
                        'department_name'=>'Requirement Analyst Team',
                        'total_members' => 10,
                        'description'=>"Prioritize requirements from various stakeholders. Communicate, translate, and simplify business requirements to ensure buy-in from all stakeholders. Assess change-proposals and define solutions to help the organization achieve its goals.",    
                    ],
                    [
                        'department_id'=>'pm_1',
                        'department_name'=>'Project Manager',
                        'total_members' => 10,
                        'description'=>"IT project managers are responsible for planning, organizing, allocating resources for, budgeting, and successfully executing organizations' specific IT goals..",
                    ],
                    [
                        'department_id'=>'pl_1',
                        'department_name'=>'Project Lead',
                        'total_members' => 10,
                        'description'=>"A project leader is a professional who leads people and makes sure a project is carried through. The project leader engages the team, motivating them, taking care of their needs and maintaining a friendly and productive work environment.",
                    ],
                    [
                        'department_id'=>'sa_1',
                        'department_name'=>'Solution Architect',
                        'total_members' => 10,
                        'description'=>"A solution architect is the person in charge of leading the practice and introducing the overall technical vision for a particular solution. While the practice can be managed in-house, there are companies that provide solution architecture consulting as a specific set of services.",
                    ],
                    [
                        'department_id'=>'ui_1',
                        'department_name'=>'UI/UX Developer',
                        'total_members' => 10,
                        'description'=>"A UI UX agency is a ux consulting company that is highly specialized in producing a quality user experience for a given digital product. Typically they are capable of producing the design and architecture of a marketing website, mobile user interfaces, and in some cases, B2B software.",
                    ],
                    [
                        'department_id'=>'wd_1',
                        'department_name'=>'Web Developer',
                        'total_members' => 10,
                        'description'=>"A Web Developer is responsible for the coding, design and layout of a website according to a company's specifications. As the role takes into consideration user experience and function, a certain level of both graphic design and computer programming is necessary.",
                    ],
                    [
                        'department_id'=>'qa_1',
                        'department_name'=>'Quality Assurance Engineer',
                        'total_members' => 10,
                        'description'=>"Quality Assurance Engineers are responsible for assessing the quality of specifications and technical design documents in order to ensure timely, relevant and meaningful feedback. They are involved in planning and implementing strategies for quality management and testing.",
                    ],
                ]
            ];

        foreach ($data as $keyd=>$valued) {
             
                    DB::table('department')->insert($valued);
                    $this->command->info($keyd);//gives you an idea where your iterator is in command line, best feeling in the world to see it rising if you ask me :D
                }
    }
}
