@extends('template.master')
@section('content')
    
    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{url('dashboard/memberinfo')}}">Memberlist</a></li>
            </ol>
        </div>
    </div>
            

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <a href="{{url('dashboard/employeelist/show/Technical')}}"><img src="{{asset('/assets/images/technical.jpg') }}" height="60%" width="60%"/>Technical Team</a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <a href="{{url('dashboard/employeelist/show/Management')}}"><img src="{{asset('/assets/images/download.jpg') }}" height="60%" width="40%"/>Management Team</a>
                    </div>
                </div>
            </div>
        </div>
        
            <!-- #/ container -->
    </div>
        <!--**********************************
            Content body end

            ***********************************-->  
            
<script>
    $(document).ready(function(){

        function clear_icon(){
            $('#employee_id').html('');
            $('#employee_name').html('');
            $('#employee_dept_id').html('');
            $('#employee_phone_number').html('');
            $('#email').html('');
            $('#address').html('');
        }

        function fetch_data(page, sort_type, sort_by, query){
            var url ="{{url('/dashboard/memberslist/fetchdata')}}";
            $.ajax({
                    type: "POST",
                    data: {"_token": "{{ csrf_token() }}","page":page,"sort_type":sort_type,"sort_by":sort_by,"search":query },
                    url: url,
                    success: function(msg){
                        $('tbody').html('');
                        $('tbody').html(msg);
                    }
            });
        }

        $(document).on('keyup', '#search', function(){
            var query = $('#search').val();
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var page = $('#hidden_page').val();

            fetch_data(page, sort_type, column_name, query);
        });

        $(document).on('click', '.sorting', function(){
            var column_name = $(this).data('column_name');
            var order_type = $(this).data('sorting_type');
            var reverse_order = '';
            if(order_type == 'asc'){
                $(this).data('sorting_type', 'desc');
                reverse_order = 'desc';
                clear_icon();
                $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-bottom"></span>');
            }
            if(order_type == 'desc'){
                $(this).data('sorting_type', 'asc');
                reverse_order = 'asc';
                clear_icon();
                $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-top"></span>');
            }

            $('#hidden_column_name').val(column_name);
            $('#hidden_sort_type').val(reverse_order);
            var page = $('#hidden_page').val();
            var query = $('#search').val();
            fetch_data(page, reverse_order, column_name, query);
        });

        $(document).on('click', '.pagination a', function(event){
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            $('#hidden_page').val(page);
            var column_name = $('#hidden_column_name').val();
            var sort_type = $('#hidden_sort_type').val();
            var query = $('#search').val();
            $('li').removeClass('active');
            $(this).parent().addClass('active');
            fetch_data(page, sort_type, column_name, query);
        });
    });
</script>
@stop


