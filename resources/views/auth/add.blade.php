@include('template.header')

        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <div class="header-left">
                   
                </div>
                <div class="header-right">
                    <ul class="clearfix">
                        
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="{{ asset('/assets/images/user/1.png') }}" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                       
                                        <hr class="my-2">
                                        
                                        <li><a href="{{url('/logout')}}"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->
       @include('template/sliderbar')

       
            <!-- row -->
            <div class="container-fluid">
                <div class="row">
                   <div class="col-11" style="margin: auto;-webkit-transform: translateX(120px);-moz-transform: translateX(120px); -ms-transform: translateX(120px); transform: translateX(120px);position:relative; left:40px; top:-5px;">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-title">Product Add</div>
                                <div class="form-validation">
                                     @if (count($errors) > 0)

                                   <div class="alert alert-danger">

                                    <ul>

                                      @foreach ($errors->all() as $error)

                                         <li>{{ $error }}</li>

                                      @endforeach

                                      </ul>

                                    </div>

                                @endif
                                    <form class="form-valide"  method="post" action="{{url('/add')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label"  for="ProductName">Product Name <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" name="productname">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" name="Price" for="Price">Price<span class="text-danger">*</span>
                                                </label>
                                            <div class="col-lg-6">
                                               <input type="number" name="productprice">  
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label"  for="Price">Image<span class="text-danger">*</span>
                                                </label>
                                            <div class="col-lg-6">
                                               <input type="file" id="imgInp" name="file"> 
                                               <img id="logo_image" src="#" alt="display image" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
   @include('template/footer')