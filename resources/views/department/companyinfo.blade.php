@if(isset($departmentlist))
    @foreach($departmentlist as $key=>$data)
      @php $department_id= Crypt::encrypt($data->department_id); @endphp
                 <!--Encrypt ID and store as $prodID-->
        <tr>
            <td><img id="preview-image-before-upload" src="{{asset('assets/images/public/'.$data->image)}}"
                                                alt="preview image" style="max-height: 100px;"></td>
            <td>{{$data->department_name}}</td>
            <td>{{$data->description}}</td>
            <td>{{$data->total_members}}</td>
            <td>
            @if(request()->has('trashed'))
                <a href="{{ route('posts.restore', $department_id) }}" class="btn btn-success">Restore</a>
            @else
                <form method="POST" action="{{ route('posts.destroy', $department_id) }}">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <button type="submit" class="btn btn-danger delete" title='Delete'><i class="fa fa-close color-danger"></i></button>
                </form>
            @endif
            <span><a href="{{url('dashboard/department/update',$department_id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a></span></td>
        </tr>
    @endforeach
    <tr>
       <td colspan="5" align="center">
        {{$departmentlist->links('pagination::bootstrap-4')}}
       </td>
@endif
                                       