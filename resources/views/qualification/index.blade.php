@extends('template.master')
@section('content')
    
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{url('dashboard/memberinfo')}}">Memberlist</a></li>
                    </ol>
                </div>
            </div>
            

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">

                              <h3 align="center">Qualification List</h3><br />
   <div class="row">
    <div class="col-md-12">

    </div>
    <div class="col-md-3">
     <div class="form-group">
         @if(Session::has('success'))

    <div class="alert alert-success" id="alert">
        <strong>Success:</strong> {{Session::get('success')}}
    </div>

@elseif(session('error'))
    <div class="alert alert-danger" id="alert">
        
        <strong>Error:</strong>{{Session::get('error')}}
    </div>
@endif
     </div>
    </div>
     <div class="col-md-3">
     <div class="form-group">
     <span><a href="{{url('dashboard/qualification/create')}}"><button type="submit" class="btn btn-primary">Add</button></a>
     </div>
    </div>
    <div class="col-md-3">
     <div class="form-group">
      <input type="text" name="search" id="search" class="form-control" />
     </div>
    </div>
   </div>
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th width="5%" class="sorting" data-sorting_type="asc" data-column_name="employee_id" style="cursor: pointer">id <span id="employee_id"></span></th>
       <th width="38%" class="sorting" data-sorting_type="asc" data-column_name="employee_name" style="cursor: pointer">name <span id="employee_name"></span></th>
       <th width="38%" class="sorting" data-sorting_type="asc" data-column_name="employee_dept_id" style="cursor: pointer">detail <span id="employee_dept_id"></span></th>
       <th width="38%" class="sorting" data-sorting_type="asc" data-column_name="employee_phone_no" style="cursor: pointer">status <span id="employee_phone_no"></span></th>
       <th width="57%">Action</th>
      </tr>
     </thead>
     <tbody>
       @include('qualification.qualificationlist')
     </tbody>
    </table>
    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="employee_id" />
    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
   </div>
  </div>  
                              

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end

            ***********************************-->  
 
    <script type="text/javascript">
            $(document).ready(function() {
                $('.delete').click(function(e) {
                    if(!confirm('Are you sure you want to delete this post?')) {
                        e.preventDefault();
                    }
                });
            });
        </script>           
<script>
$(document).ready(function(){

 function clear_icon()
 {
  $('#employee_id').html('');
  $('#employee_name').html('');
  $('#employee_dept_id').html('');
  $('#employee_phone_number').html('');
  $('#email').html('');
  $('#address').html('');
 }

 function fetch_data(page, sort_type, sort_by, query)
 {
    var url ="{{url('/dashboard/membersinfo/fetchdata')}}";
  $.ajax({

   type: "POST",

   data: {"_token": "{{ csrf_token() }}","page":page,"sort_type":sort_type,"sort_by":sort_by,"search":query },

   url: url,
   success: function(msg){

    $('tbody').html('');
    $('tbody').html(msg);
   }
  });
 }

 $(document).on('keyup', '#search', function(){
  var query = $('#search').val();
  var column_name = $('#hidden_column_name').val();
  var sort_type = $('#hidden_sort_type').val();
  var page = $('#hidden_page').val();

  fetch_data(page, sort_type, column_name, query);
 });

 $(document).on('click', '.sorting', function(){
  var column_name = $(this).data('column_name');
  var order_type = $(this).data('sorting_type');
  var reverse_order = '';
  if(order_type == 'asc')
  {
   $(this).data('sorting_type', 'desc');
   reverse_order = 'desc';
   clear_icon();
   $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-bottom"></span>');
  }
  if(order_type == 'desc')
  {
   $(this).data('sorting_type', 'asc');
   reverse_order = 'asc';
   clear_icon
   $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-top"></span>');
  }
  $('#hidden_column_name').val(column_name);
  $('#hidden_sort_type').val(reverse_order);
  var page = $('#hidden_page').val();
  var query = $('#search').val();
  fetch_data(page, reverse_order, column_name, query);
 });

 $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  $('#hidden_page').val(page);
  var column_name = $('#hidden_column_name').val();
  var sort_type = $('#hidden_sort_type').val();

  var query = $('#search').val();

  $('li').removeClass('active');
        $(this).parent().addClass('active');
  fetch_data(page, sort_type, column_name, query);
 });

});
</script>
@stop


