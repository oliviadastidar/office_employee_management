<?php

namespace App\Http\Controllers;
use App\Models\Department;
use App\Models\Employee;
use App\Models\EmployeeCoordinator;
use App\Models\Qualification;
use App\Models\EmployeeProfessionalDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
// use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use App\Http\Middleware\ispm;
class EmployeeListController extends Controller
{
//     public function __construct()
//    {
//       $this->middleware(ispm::class);
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('employeelist.index');  
    }
       

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function proffesionaldatastore(Request $request){
        $id = Crypt::decrypt($request->employee_id);
         $validator = validator()->make(request()->all(), [
                        'department_name'=>'required',
                        'employee_designation' => 'required|regex:/^[a-zA-Z\s]*$/',
                         'employee_experience_in_previous_company' => 'required|regex:/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/',
                        'employee_experience_in_our_company' => 'required|regex:/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/',
                        'employee_qualification' => 'required|integer',
                        'employee_technical_skill'=> 'required', 
        ]);
       
        if ($validator->fails())
        {
          return back()->withErrors($validator)->withInput();
        }else{
            
            $array = array(
                        'designation'=>$request->employee_designation,
                        'experience_in_previous_company'=>$request->employee_experience_in_previous_company,
                        'experience_in_our_company'=>$request->employee_experience_in_our_company,
                        'qualification'=>$request->employee_qualification,
                        'technical_skills'=>$request->employee_technical_skill,
            );
            
            $update = EmployeeProfessionalDetail::scopeUpdateemployee($id, $array);
        }
           if($update){
            return back()->with('success','data updated successfully');
           }else{
            return back()->with('fail','data updation not successful');
           }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $id = Crypt::decrypt($request->employee_id);
        $validator = validator()->make(request()->all(), [
                        'employee_name' => 'required|regex:/^[\pL\s\-]+$/u|unique:employee,employee_name,'.$id.',employee_id',
                         'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                        'gender' => 'required',
                        'employee_phone_no'=> 'required|integer', 
                        'email' => 'required|email|unique:employee,email,'.$id.',employee_id',
        ]);
        
        if ($validator->fails())
        {
          return back()->withErrors($validator)->withInput();
        }else{ 
             $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('assets/images/public/'), $imageName);
            $array = array(
                        'employee_id'=> $id,
                        'image' =>$imageName,
                        'gender'=>$request->gender,
                        'employee_phone_no'=>$request->employee_phone_no,
                        'email'=>$request->email,
            );
           $update = Employee::scopeUpdateemployee($id,$array);
           if($update){
            return back()->with('success','data updated successfully');
           }else{
            return back()->with('fail','data updation not successful');
           }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //user choice
        if($id != ''){
          $department_type = $id;
        }else{
          $department_type = 'Technical';
        }
        
        //array list depend on the department
        if($department_type)
        {
            if($department_type == "Technical"){
                $dept_Array = ['pm_1','pl_1','wd_1','qa_1','ra_1','sa_1','ui_1'];
            
            }else{
                 $dept_Array = ['f_1','hr_1'];
                
            }
        }
       
        $department_detail = Department::scopeGetalldata($dept_Array); //department detail list
        
        foreach($department_detail as $key=>$value){
           
           $dept_employee = Employee::scopeEmployeelist($value[0]->department_id); //employee information

           $response_array[] = array('department_detail'=>$value[0],'list'=>$dept_employee,);
        }
        $final_response = $response_array;
        //dd($final_response);
        return view('employeelist.employeeinfo',compact('department_type','final_response'));
    }

    public function upload(Request $request){

        $request->validate([
            'releaseletter' => 'required|mimes:pdf,xlx,csv',
        ]);
        
        $fileName = $request->input('choice').time().'.'.$request->releaseletter->extension();  
        $id = Crypt::decrypt($request->employee_id);
         
        $request->releaseletter->move(public_path('uploads/professional/documents'), $fileName);
        $array = array(
                        $request->input('choice')=> $fileName,
            );
           $update = EmployeeProfessionalDetail::scopeUpdateemployee($id,$array);
        //    die('i am just tired and exhausted');
           if($update){
            return back()->with('success','You have successfully upload file.');
           }else{
            return back()->with('fail','data updation not successful');
           }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
        $id = Crypt::decrypt($request->employee_id);
        $department_list = Department::getlist();
        $qualification =Qualification::getlist();
        $employeepersonaldetails = Employee::getPersonalDetails($id);
        $employeeprofessionaldetails = EmployeeProfessionalDetail::getemployeeprofessionaldetails($id);
        $employeedepartmentid = Employee::getdepartmentid($id);
        $employeedepartmentname = Department::getdepartmentname($employeedepartmentid);
        
        return view('employeelist.add_edit',compact('department_list','qualification','employeepersonaldetails','employeeprofessionaldetails','employeedepartmentname'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getfilterdata(Request $request){
        $department_type = '';
      
        $list = new Collection(['pm_1','pl_1','wd_1','qa_1','ra_1','sa_1','ui_1']);
            $id = $request->input('dept');
            if ($list->contains($id)) {
                //yes: $id exits in array
                $department_type = 'Technical';
            }else{
                $department_type = 'Management';
            }

        if($request->ajax())
        {
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
            $query = $request->input('search');
            $query = str_replace(" ", "%", $query);
            $dept = $id;
            $list =Employee::filterdata($sort_by,$sort_type,$query,$dept);

           $final_response = $list;
           
        } else{
            $final_response = "data not available";
        }  

        return $final_response;
        
       }
}
