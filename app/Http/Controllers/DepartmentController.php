<?php

namespace App\Http\Controllers;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Http\Middleware\isaccess;
class DepartmentController extends Controller
{
  public function __construct()
   {
      $this->middleware(isaccess::class);
   }  

    public function index(Request $request){
        $departmentlist = [];
        if ($request->has('trashed')) {
          
            $departmentlist = Department::getthrashed();
        } else {
           $departmentlist = Department::getdepartmentlist();
        }
          
        return view('department.index',compact('departmentlist'));      
    }

    public function getfilterdata(Request $request){
        if($request->ajax())
     {
      $sort_by = $request->input('sort_by');
      $sort_type = $request->input('sort_type');
            $query = $request->input('search');
            $query = str_replace(" ", "%", $query);
      $departmentlist =Department::filterdata($sort_by,$sort_type,$query);
     
    //    
      return view('department.companyinfo', compact('departmentlist'))->render();
     }
    }
    public function create(){
       return view('department.add_edit');

    }
    public function edit($department_id){
       $id = Crypt::decrypt($department_id);
       $getdetail = Department::getiddetail($id);
    
       return view('department.add_edit',compact('getdetail'));
    }

    public function store(Request $request){
       
       if($request->department_id){
          $id = Crypt::decrypt($request->department_id);
       }else{
         $id= '';
       }
        $validator = validator()->make(request()->all(), [
                        'image' => 'required|mimes:jpeg,png,jpg,gif,svg',
                        'department_name' => 'required|regex:/^[\pL\s\-]+$/u|unique:department,department_name,'.$id.',department_id',
                        'description' => 'required',
                        'total_members' => 'required|integer',
        ]);
        
        if ($validator->fails())
        {
          return back()->withErrors($validator)->withInput();
        }else{
             $imageName = time().'.'.$request->image->extension();  
     
          $request->image->move(public_path('assets/images/public/'), $imageName);
          if($request->department_id != ''){
            
           $response = Department::updatedepartment($id,$request->all(),$imageName);   
          }else{
            $response = Department::updatedepartment('',$request->all(),$imageName);
          }
        }

        if($response){
          //$status = "Department updated successfully";
           return redirect(url('dashboard/department'))->with('success', 'Data Your Comment has been created successfully'); //route('dashboard')->with('success',"Department updated successfully"); 
           
        }else{
          return back()->withErrors('Something Went Wrong.Please Try Again Later');
        }
    }

    public function destroy($department_id)
    {
       $id = Crypt::decrypt($department_id);
        $cleardata = Department::destroy($id);
  
        return redirect()->back();
    }

    public function restore($department_id)
    {
       $id = Crypt::decrypt($department_id);
      $restore = Department::restoreid($id);
  
        return redirect()->back();
    }

    public function restoreAll()
    {
        $restoreall = Department::restoreall();
  
        return redirect()->back();
    }
}
