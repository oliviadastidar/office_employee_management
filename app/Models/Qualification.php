<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    use HasFactory;
    protected $table = 'qualification';

    protected $fillable = ['name','detail','status'];

    public static function getlist(){
        $response = Qualification::all('id','name');
        return $response;
    }

    public static function getinfo($id){
        $response = Qualification::where('id',$id)->get();
        return $response;
    }

    public static function scopeInsert($arr){
        
         $response = new Qualification;
        $response->name = $arr['name'];
        $response->detail = $arr['detail'];
        $response->status = 0;
        
        $response->save();
        
        return $response;
    }

    public static function scopeUpdate($arr,$id){
        $array = ['name'=>$arr['name'],'detail'=>$arr['detail']];
        $response = Qualification::where('id', $id)  // find your user by their email
        ->update($array); 
        return $response;
    }

    public static function getthrashed(){
        $response = Qualification::onlyTrashed()
                ->get();

                return $response;
    }
      public static function destroy($id){
        $response =Qualification::find($id)->delete();
        return $response;
    }
    public static function restoreid($id){
        $restore = Qualification::withTrashed()->find($id)->restore();
        return $restore;
    }
    public static function restoreall(){
        $restore = Qualification::onlyTrashed()->restore();
         return $restore;
    }
}
