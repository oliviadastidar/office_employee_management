<?php

namespace App\Http\Controllers;
use App\Models\Qualification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\Http\Middleware\isaccess;
class QualificationController extends Controller
{
    public function __construct()
   {
      $this->middleware(isaccess::class);
   }
    public function index(){
       $list = Qualification::getlist(); 
       return view('qualification.index',compact('list'));
    }

     public function edit(Request $request)
    {
        if($request->id){
            $id = Crypt::decrypt($request->id);
            $qualificationdetail =Qualification::getinfo($id);
            return view('qualification.add_edit',compact('qualificationdetail'));
        }else{
            $id = '';
            return view('qualification.add_edit');
        }
    }

    public function create(){
        return view('qualification.add_edit');
    }
    
    public function store(Request $request){
        if($request->qualification_id){
            $id = Crypt::decrypt($request->qualification_id);
        }else{
            $id = '';
        }

        if($id == ''){
            $response =Qualification::scopeInsert($request->all());
        }else{
            $response = Qualification::scopeUpdate($request->all(),$id);
        }
        
        if($response){
            return redirect('/dashboard/qualification');
        }else{
            return back()->with('Error','data updation not successful');
        }
    }

     public function destroy($qualification_id)
    {
        //
        $id = Crypt::decrypt($qualification_id);
        $cleardata = Qualification::destroy($id);
         
        return redirect()->back();
    }

    public function restore($qualification_id)
    {
       $id = Crypt::decrypt($qualification_id);
      $restore = Qualification::restoreid($id);
  
        return redirect()->back();
    }

    public function restoreAll()
    {
        $restoreall = Qualification::restoreall();
  
        return redirect()->back();
    }
}
