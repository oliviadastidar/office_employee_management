-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2022 at 08:18 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_members` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_name`, `total_members`, `description`, `created_at`, `updated_at`) VALUES
('f_1', 'Finance Team', '3', 'The finance department ensures the adequate and timely provision of funds for the business\'s operations.', NULL, NULL),
('hr_1', 'Human Resource Team', '6', 'Human resources (HR) is the division of a business that is charged with finding, screening, recruiting, and training job applicants, as well as administering employee-benefit programs.', NULL, NULL),
('pl_1', 'Project Lead', '9', 'A project leader is a professional who leads people and makes sure a project is carried through. The project leader engages the team, motivating them, taking care of their needs and maintaining a friendly and productive work environment.', NULL, NULL),
('pm_1', 'Project Manager', '3', 'IT project managers are responsible for planning, organizing, allocating resources for, budgeting, and successfully executing organizations\' specific IT goals..', NULL, NULL),
('qa_1', 'Quality Assurance Engineer', '6', 'Quality Assurance Engineers are responsible for assessing the quality of specifications and technical design documents in order to ensure timely, relevant and meaningful feedback. They are involved in planning and implementing strategies for quality management and testing.', NULL, NULL),
('ra_1', 'Requirement Analyst Team', '6', 'Prioritize requirements from various stakeholders. Communicate, translate, and simplify business requirements to ensure buy-in from all stakeholders. Assess change-proposals and define solutions to help the organization achieve its goals.', NULL, NULL),
('sa_1', 'Solution Architect', '6', 'A solution architect is the person in charge of leading the practice and introducing the overall technical vision for a particular solution. While the practice can be managed in-house, there are companies that provide solution architecture consulting as a specific set of services.', NULL, NULL),
('ui_1', 'UI and UX Developer', '6', 'A UI UX agency is a ux consulting company that is highly specialized in producing a quality user experience for a given digital product. Typically they are capable of producing the design and architecture of a marketing website, mobile user interfaces, and in some cases, B2B software.', NULL, NULL),
('wd_1', 'Web Developer', '18', 'A Web Developer is responsible for the coding, design and layout of a website according to a company\'s specifications. As the role takes into consideration user experience and function, a certain level of both graphic design and computer programming is necessary.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_phone_no` bigint(80) NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gender` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `employee_name`, `employee_phone_no`, `email`, `address`, `image`, `created_at`, `updated_at`, `gender`) VALUES
('cd7be9e2-8a4d-4221-a6d1-c53acbd2984b', 'Alain Campos', 6333333333, 'AlainCampos@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('49834304-b60e-42d2-b9e9-cbd6be460dfd', 'Albert Scariachan', 2000000000, 'AlbertScariachan@gma', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('195b408b-d6dc-4958-a725-446e0a0c2835', 'Alexander Borges', 7222222222, 'AlexanderBorges@gmail.com', 'Los Angeles, California, United Kingdom.', '1645598949.jpg', NULL, '2022-02-23 01:19:09', 'male'),
('b5ade706-d61d-4068-b1fb-9c83c30ac2f5', 'Alex Pathrose', 90444444444, 'AlexPathrose@gmail.c', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('6b3ed824-971b-4e89-b0f2-db8a03080c29', 'Amara Rebelo', 34444444444, 'AmaraRebelo@gmail.co', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('3250fc80-13de-4ea1-af8c-6b67568569aa', 'Amelia Chavara', 6555555555, 'AmeliaChavara@gmail.', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('ea23aea7-57c3-42ed-a5e5-ae92229e9bda', 'Amelia Quadros', 9009999999, 'AmeliaQuadroso@gmail', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('88d6cf1c-ba7f-40ce-9cdf-07756ecd0284', 'Ammy Varghese', 2222222222, 'AmmyVarghese@gmail.c', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('28d1f803-9eba-474d-b36f-5eb757aee4a2', 'Andrew Oliveira', 3111111111, 'AndrewOliveira@gmail', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('d7d79d34-a2c9-4530-944c-6ae80912d8f4', 'Ariel Xavier', 2444444444, 'ArielXavier@gmail.co', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('4ed63716-c27c-4f42-a5f6-2764bb26e392', 'Ava Alfonso', 9222222222, 'AvaAlfonso@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('fff4f545-2ee5-4475-9823-38e906e0bc56', 'Ava Peeri', 90222222222, 'AvaPeeri@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('e58dec16-6e9f-406f-b166-91aa8e172a1b', 'Benjamin Andrade', 8111111111, 'BenjaminAndrade@gmai', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('4062d341-e917-40d9-a7bd-682bc8fe4df5', 'Benjamin Noronha', 9077777777, 'BenjaminNoronha@gmai', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('08d15a32-f8c7-4e00-a635-d2d4ba228891', 'Ben Mathaai', 4333333333, 'BenMathaai@gmail.com', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('612a864d-4b04-49cf-ac96-eaa3b2761e6b', 'Betty Thomachan', 2111111111, 'BettyThomachan@gmail', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('3173fc54-b125-4549-9d51-0a66dab1465c', 'Charlotte Amaral', 9555555555, 'CharlotteAmaral@gmai', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('003943a6-eae5-43f4-bf19-3c71606c1e64', 'Charlotte Pereira', 90111111111, 'CharlottePereira@gma', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('74f2f9d7-f752-4629-a61a-47501ca30b2f', 'Edward Carneiro', 6444444444, 'EdwardCarneiro@gmail', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('b46af98a-b83a-48d5-802c-03283530b80a', 'Elanor Sequeira', 1777777777, 'ElanorSequeira@gmail', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('960cb2cd-a664-4dbc-8b0b-7c9a4a83aff2', 'Elijah Alvares', 9333333333, 'ElijahAlvares@gmail.', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('a744ebba-1573-45df-ae3b-773c3899ae46', 'Elijah Rodrigues', 1888888888, 'ElijahRodrigues@gmai', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('09024e42-0774-4422-8f32-38e2397ae96a', 'Ella Geevarghese', 5000000000, 'EllaGeevarghese@gmai', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('89acba24-47a0-4ab3-ad92-28474b2ff14d', 'Emma Alberto', 9111111111, 'EmmaAlberto@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('ed28918f-5e87-4572-98d9-35cacd6cff96', 'Emma Paulose', 90333333333, 'EmmaPaulose@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('459cfe38-5d56-4ef7-98fd-2188a67b2464', 'Evelyn Figueira', 6888888888, 'EvelynFigueira@gmail', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('3a2eb97d-74a3-4fd1-b5ae-b1fa433f924c', 'Finn Kolan', 5444444444, 'FinnKolan@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('d01905fb-f402-4247-b355-e73bf7b727b0', 'Haward Ouseph', 3222222222, 'HawardOuseph@gmail.c', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('c6e2f60a-9323-4ed7-ad91-ef23390798e5', 'Helen Mariamma', 4000000000, 'HelenMariamma@gmail.', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('4f58fa29-9651-46bd-a664-7f17536ca402', 'Henry Batista', 7111111111, 'HenryBatista@gmail.c', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('97caf874-5c86-4b67-a5a7-b1ef24c4a33a', 'Henry Ouseph', 90555555555, 'HenryOuseph@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('049e40b8-a4bb-4eaa-ae8e-8ebe61b79dcc', 'Innana Martin', 4222222222, 'InnanaMartin@gmail.c', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('f7b9c428-6520-4f30-b192-5f24a86eaaa8', 'Isabella Fernandez', 6666666666, 'IsabellaFernandez@gm', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('85055755-3d88-4614-aba6-53367b247be6', 'James Nazareth', 9088888888, 'JamesNazaretha@gmail', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('e75e0a6a-ecc9-4adb-ba28-68efa6bbfa54', 'Jasmine Gonsalves', 5222222222, 'JasmineGonsalves@gma', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('4b28c6c1-629b-49ee-a1a1-7df8bc309c76', 'Julian Cabral', 6111111111, 'JulianCabral@gmail.c', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('ea466963-766b-4f64-9f43-36eba112c512', 'Karen Lopez', 5555555555, 'KarenLopez@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('963369b3-16c3-433c-b459-e4b730768152', 'Katherine Pinto', 3333333333, 'KatherinePinto@gmail', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('7947eda8-4f9d-48f3-b302-de88f15f222f', 'Lena Mariam', 4111111111, 'LenaMariam@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('9841a4e2-6957-4d0e-a151-28759ac2ef65', 'Lenard Paulose', 4444444444, 'LenardPaulose@gmail.', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('4e4520b0-e79c-40d1-a882-451c059f99db', 'Liam Agostinho', 9000000000, 'LiamAgostinho@gmail.', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('694ed023-23dc-4acd-9f18-990bf7461162', 'Lucas Arkanj', 7000000000, 'LucasArkanj@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('a7292e76-8feb-4c35-b01b-69a0da102f7b', 'Lucas Oliveira', 90666666666, 'LucasOliveira@gmail.', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('9807ddbd-1de4-45b8-b9c5-9d0c09046bd6', 'Mathew Thommi', 1666666666, 'MathewThommi@gmail.c', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('03643192-fbf3-419d-8f01-01ba8737756e', 'Mia Ferreira', 6777777777, 'MiaFerreira@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('a0da0409-f23d-4093-931d-a0aa58369673', 'Mia Rebelo', 9007777777, 'MiaRebelo@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('1e6593a8-97dc-446b-80c1-8c586dce98a5', 'Nalini Yohanan', 1222222222, 'NaliniYohanan@gmail.', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('1a5e2365-57e1-46f6-a4a8-47649bd217b1', 'Oliver Amorin', 8000000000, 'OliverAmorin@gmail.c', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('521706e4-911a-437f-90d5-fb00f0418574', 'Olivia Yohannan', 1000000000, 'OliviaYohannan@gmail', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('de3682e2-411e-46dd-baf2-24ea61f0a426', 'Paxton Nascimento', 3000000000, 'PaxtonNascimento@gma', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('2a5a7db5-5b04-42cb-8dfa-58ab8eb5196b', 'Penny Rodrigues', 3555555555, 'PennyRodrigues@gmail', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('56d93300-0441-41d7-8666-e74bdf8bb917', 'Pete Camara', 6222222222, 'PeteCamara@gmail.com', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('d18693ee-f4b6-445c-b4e3-ff48b5b238c3', 'Ronald Henriques', 5333333333, 'RonaldHenriques@gmai', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('4ba2647f-55dd-44bc-b8ed-e2039214b508', 'Ron Yesu', 2555555555, 'RonYesu@gmail.com', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('28e105da-cc7a-4724-bf63-58c34c57055b', 'Selena Gomez', 5111111111, 'SelenaGomez@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('4b989e59-a552-4c1a-855e-8940379426f0', 'Sheldon Cooper', 4555555555, 'SheldonCooper@gmail.', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('444f4c5f-9e9f-498b-b1a8-c0faf11bee93', 'Simon Branco', 6000000000, 'SimonBranco@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('2980d8b3-964c-494a-920c-105f1b8e62cc', 'Sophia Antunes', 8222222222, 'SophiaAntunes@gmail.', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('15d96efb-1031-489a-bc69-2a6b31ef9318', 'Sophia Pinto', 9008888888, 'SophiaPinto@gmail.co', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('08aaedd5-d61d-4314-ab15-c8cbff6fb74d', 'Trent Varughese', 2333333333, 'TrentVarughese@gmail', 'Los Angeles, California, United Kingdom', NULL, NULL, NULL, NULL),
('4986cc8a-d9db-443d-8540-7f95589ba1fe', 'Tristina Varughese', 1555555555, 'TristinaVarughese@gm', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('3e2cf3df-2481-401f-bd67-f7c7a4f8067f', 'William Alves', 9444444444, 'WilliamAlves@gmail.c', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL),
('9eb34bed-1820-4b3a-aea3-eacf09bab876', 'William Rebelo', 1999999999, 'WilliamRebelo@gmail.', 'Los Angeles, California, United Kingdom.', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employeecoordinator`
--

CREATE TABLE `employeecoordinator` (
  `id` int(11) NOT NULL,
  `department_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employeecoordinator`
--

INSERT INTO `employeecoordinator` (`id`, `department_id`, `employee_id`, `created_at`, `updated_at`, `supervisor`) VALUES
(1, 'f_1', 'e58dec16-6e9f-406f-b166-91aa8e172a1b', NULL, NULL, NULL),
(2, 'f_1', '1a5e2365-57e1-46f6-a4a8-47649bd217b1', NULL, NULL, NULL),
(3, 'f_1', '2980d8b3-964c-494a-920c-105f1b8e62cc', NULL, NULL, NULL),
(4, 'hr_1', '4ed63716-c27c-4f42-a5f6-2764bb26e392', NULL, NULL, NULL),
(5, 'hr_1', '3173fc54-b125-4549-9d51-0a66dab1465c', NULL, NULL, NULL),
(6, 'hr_1', '960cb2cd-a664-4dbc-8b0b-7c9a4a83aff2', NULL, NULL, NULL),
(7, 'hr_1', '89acba24-47a0-4ab3-ad92-28474b2ff14d', NULL, NULL, NULL),
(8, 'hr_1', '4e4520b0-e79c-40d1-a882-451c059f99db', NULL, NULL, NULL),
(9, 'hr_1', '3e2cf3df-2481-401f-bd67-f7c7a4f8067f', NULL, NULL, NULL),
(10, 'pl_1', 'cd7be9e2-8a4d-4221-a6d1-c53acbd2984b', NULL, NULL, NULL),
(11, 'pl_1', '3250fc80-13de-4ea1-af8c-6b67568569aa', NULL, NULL, NULL),
(12, 'pl_1', '74f2f9d7-f752-4629-a61a-47501ca30b2f', NULL, NULL, NULL),
(13, 'pl_1', '459cfe38-5d56-4ef7-98fd-2188a67b2464', NULL, NULL, NULL),
(14, 'pl_1', 'f7b9c428-6520-4f30-b192-5f24a86eaaa8', NULL, NULL, NULL),
(15, 'pl_1', '4b28c6c1-629b-49ee-a1a1-7df8bc309c76', NULL, NULL, NULL),
(16, 'pl_1', '03643192-fbf3-419d-8f01-01ba8737756e', NULL, NULL, NULL),
(17, 'pl_1', '56d93300-0441-41d7-8666-e74bdf8bb917', NULL, NULL, NULL),
(18, 'pl_1', '444f4c5f-9e9f-498b-b1a8-c0faf11bee93', NULL, NULL, NULL),
(19, 'pm_1', '195b408b-d6dc-4958-a725-446e0a0c2835', NULL, NULL, NULL),
(20, 'pm_1', '4f58fa29-9651-46bd-a664-7f17536ca402', NULL, NULL, NULL),
(21, 'pm_1', '694ed023-23dc-4acd-9f18-990bf7461162', NULL, NULL, NULL),
(22, 'qa_1', '09024e42-0774-4422-8f32-38e2397ae96a', NULL, NULL, NULL),
(23, 'qa_1', '3a2eb97d-74a3-4fd1-b5ae-b1fa433f924c', NULL, NULL, NULL),
(24, 'qa_1', 'e75e0a6a-ecc9-4adb-ba28-68efa6bbfa54', NULL, NULL, NULL),
(25, 'qa_1', 'ea466963-766b-4f64-9f43-36eba112c512', NULL, NULL, NULL),
(26, 'qa_1', 'd18693ee-f4b6-445c-b4e3-ff48b5b238c3', NULL, NULL, NULL),
(27, 'qa_1', '28e105da-cc7a-4724-bf63-58c34c57055b', NULL, NULL, NULL),
(28, 'ra_1', '08d15a32-f8c7-4e00-a635-d2d4ba228891', NULL, NULL, NULL),
(29, 'ra_1', 'c6e2f60a-9323-4ed7-ad91-ef23390798e5', NULL, NULL, NULL),
(30, 'ra_1', '049e40b8-a4bb-4eaa-ae8e-8ebe61b79dcc', NULL, NULL, NULL),
(31, 'ra_1', '7947eda8-4f9d-48f3-b302-de88f15f222f', NULL, NULL, NULL),
(32, 'ra_1', '9841a4e2-6957-4d0e-a151-28759ac2ef65', NULL, NULL, NULL),
(33, 'ra_1', '4b989e59-a552-4c1a-855e-8940379426f0', NULL, NULL, NULL),
(34, 'sa_1', '6b3ed824-971b-4e89-b0f2-db8a03080c29', NULL, NULL, NULL),
(35, 'sa_1', '28d1f803-9eba-474d-b36f-5eb757aee4a2', NULL, NULL, NULL),
(36, 'sa_1', 'd01905fb-f402-4247-b355-e73bf7b727b0', NULL, NULL, NULL),
(37, 'sa_1', '963369b3-16c3-433c-b459-e4b730768152', NULL, NULL, NULL),
(38, 'sa_1', 'de3682e2-411e-46dd-baf2-24ea61f0a426', NULL, NULL, NULL),
(39, 'sa_1', '2a5a7db5-5b04-42cb-8dfa-58ab8eb5196b', NULL, NULL, NULL),
(40, 'ui_1', '49834304-b60e-42d2-b9e9-cbd6be460dfd', NULL, NULL, NULL),
(41, 'ui_1', '88d6cf1c-ba7f-40ce-9cdf-07756ecd0284', NULL, NULL, NULL),
(42, 'ui_1', 'd7d79d34-a2c9-4530-944c-6ae80912d8f4', NULL, NULL, NULL),
(43, 'ui_1', '612a864d-4b04-49cf-ac96-eaa3b2761e6b', NULL, NULL, NULL),
(44, 'ui_1', '4ba2647f-55dd-44bc-b8ed-e2039214b508', NULL, NULL, NULL),
(45, 'ui_1', '08aaedd5-d61d-4314-ab15-c8cbff6fb74d', NULL, NULL, NULL),
(46, 'wd_1', 'b5ade706-d61d-4068-b1fb-9c83c30ac2f5', NULL, NULL, NULL),
(47, 'wd_1', 'ea23aea7-57c3-42ed-a5e5-ae92229e9bda', NULL, NULL, NULL),
(48, 'wd_1', 'fff4f545-2ee5-4475-9823-38e906e0bc56', NULL, NULL, NULL),
(49, 'wd_1', '4062d341-e917-40d9-a7bd-682bc8fe4df5', NULL, NULL, NULL),
(50, 'wd_1', '003943a6-eae5-43f4-bf19-3c71606c1e64', NULL, NULL, NULL),
(51, 'wd_1', 'b46af98a-b83a-48d5-802c-03283530b80a', NULL, NULL, NULL),
(52, 'wd_1', 'a744ebba-1573-45df-ae3b-773c3899ae46', NULL, NULL, NULL),
(53, 'wd_1', 'ed28918f-5e87-4572-98d9-35cacd6cff96', NULL, NULL, NULL),
(54, 'wd_1', '97caf874-5c86-4b67-a5a7-b1ef24c4a33a', NULL, NULL, NULL),
(55, 'wd_1', '85055755-3d88-4614-aba6-53367b247be6', NULL, NULL, NULL),
(56, 'wd_1', 'a7292e76-8feb-4c35-b01b-69a0da102f7b', NULL, NULL, NULL),
(57, 'wd_1', '9807ddbd-1de4-45b8-b9c5-9d0c09046bd6', NULL, NULL, NULL),
(58, 'wd_1', 'a0da0409-f23d-4093-931d-a0aa58369673', NULL, NULL, NULL),
(59, 'wd_1', '1e6593a8-97dc-446b-80c1-8c586dce98a5', NULL, NULL, NULL),
(60, 'wd_1', '521706e4-911a-437f-90d5-fb00f0418574', NULL, NULL, NULL),
(61, 'wd_1', '15d96efb-1031-489a-bc69-2a6b31ef9318', NULL, NULL, NULL),
(62, 'wd_1', '4986cc8a-d9db-443d-8540-7f95589ba1fe', NULL, NULL, NULL),
(63, 'wd_1', '9eb34bed-1820-4b3a-aea3-eacf09bab876', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employeeprofessionaldetail`
--

CREATE TABLE `employeeprofessionaldetail` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_in_previous_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `experience_in_our_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `graduation_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_graduate_certificate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employeeprofessionaldetail`
--

INSERT INTO `employeeprofessionaldetail` (`id`, `employee_id`, `designation`, `experience_in_previous_company`, `experience_in_our_company`, `qualification`, `graduation_certificate`, `post_graduate_certificate`, `created_at`, `updated_at`) VALUES
(10, '003943a6-eae5-43f4-bf19-3c71606c1e64', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '03643192-fbf3-419d-8f01-01ba8737756e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '049e40b8-a4bb-4eaa-ae8e-8ebe61b79dcc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '08aaedd5-d61d-4314-ab15-c8cbff6fb74d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '08d15a32-f8c7-4e00-a635-d2d4ba228891', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '09024e42-0774-4422-8f32-38e2397ae96a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '15d96efb-1031-489a-bc69-2a6b31ef9318', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '195b408b-d6dc-4958-a725-446e0a0c2835', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '1a5e2365-57e1-46f6-a4a8-47649bd217b1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '1e6593a8-97dc-446b-80c1-8c586dce98a5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '28d1f803-9eba-474d-b36f-5eb757aee4a2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '28e105da-cc7a-4724-bf63-58c34c57055b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '2980d8b3-964c-494a-920c-105f1b8e62cc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '2a5a7db5-5b04-42cb-8dfa-58ab8eb5196b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '3173fc54-b125-4549-9d51-0a66dab1465c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '3250fc80-13de-4ea1-af8c-6b67568569aa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '3a2eb97d-74a3-4fd1-b5ae-b1fa433f924c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '3e2cf3df-2481-401f-bd67-f7c7a4f8067f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '4062d341-e917-40d9-a7bd-682bc8fe4df5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '444f4c5f-9e9f-498b-b1a8-c0faf11bee93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '459cfe38-5d56-4ef7-98fd-2188a67b2464', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '49834304-b60e-42d2-b9e9-cbd6be460dfd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '4986cc8a-d9db-443d-8540-7f95589ba1fe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, '4b28c6c1-629b-49ee-a1a1-7df8bc309c76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '4b989e59-a552-4c1a-855e-8940379426f0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, '4ba2647f-55dd-44bc-b8ed-e2039214b508', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '4e4520b0-e79c-40d1-a882-451c059f99db', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '4ed63716-c27c-4f42-a5f6-2764bb26e392', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '4f58fa29-9651-46bd-a664-7f17536ca402', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '521706e4-911a-437f-90d5-fb00f0418574', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '56d93300-0441-41d7-8666-e74bdf8bb917', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '612a864d-4b04-49cf-ac96-eaa3b2761e6b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '694ed023-23dc-4acd-9f18-990bf7461162', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '6b3ed824-971b-4e89-b0f2-db8a03080c29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '74f2f9d7-f752-4629-a61a-47501ca30b2f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '7947eda8-4f9d-48f3-b302-de88f15f222f', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '85055755-3d88-4614-aba6-53367b247be6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '88d6cf1c-ba7f-40ce-9cdf-07756ecd0284', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '89acba24-47a0-4ab3-ad92-28474b2ff14d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '960cb2cd-a664-4dbc-8b0b-7c9a4a83aff2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '963369b3-16c3-433c-b459-e4b730768152', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '97caf874-5c86-4b67-a5a7-b1ef24c4a33a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '9807ddbd-1de4-45b8-b9c5-9d0c09046bd6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '9841a4e2-6957-4d0e-a151-28759ac2ef65', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '9eb34bed-1820-4b3a-aea3-eacf09bab876', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'a0da0409-f23d-4093-931d-a0aa58369673', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'a7292e76-8feb-4c35-b01b-69a0da102f7b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'a744ebba-1573-45df-ae3b-773c3899ae46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'b46af98a-b83a-48d5-802c-03283530b80a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'b5ade706-d61d-4068-b1fb-9c83c30ac2f5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'c6e2f60a-9323-4ed7-ad91-ef23390798e5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'cd7be9e2-8a4d-4221-a6d1-c53acbd2984b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'd01905fb-f402-4247-b355-e73bf7b727b0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'd18693ee-f4b6-445c-b4e3-ff48b5b238c3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 'd7d79d34-a2c9-4530-944c-6ae80912d8f4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'de3682e2-411e-46dd-baf2-24ea61f0a426', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'e58dec16-6e9f-406f-b166-91aa8e172a1b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'e75e0a6a-ecc9-4adb-ba28-68efa6bbfa54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'ea23aea7-57c3-42ed-a5e5-ae92229e9bda', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'ea466963-766b-4f64-9f43-36eba112c512', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'ed28918f-5e87-4572-98d9-35cacd6cff96', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'f7b9c428-6520-4f30-b192-5f24a86eaaa8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'fff4f545-2ee5-4475-9823-38e906e0bc56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(17, '2014_10_12_000000_create_users_table', 1),
(18, '2014_10_12_100000_create_password_resets_table', 1),
(19, '2019_08_19_000000_create_failed_jobs_table', 1),
(20, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(21, '2022_02_07_154458_create__department_table', 1),
(22, '2022_02_08_055626_create_employee_table', 1),
(23, '2022_02_14_143902_create_employeecoordinator_table', 1),
(24, '2022_02_15_062304_supervisor_table', 1),
(25, '2022_02_17_083446_create_employeeprofessionaldetail_table', 1),
(26, '2022_02_22_061709_add_gender_table', 2),
(27, '2022_02_23_070000_rename_experience_detail_in_employeeprofesionaldetail_table', 3),
(28, '2022_02_23_070721_rename_10th_certificate_in_employeeprofesionaldetail_table', 4),
(29, '2022_02_23_070921_rename_12th_certificate_in_employeeprofesionaldetail_table', 5),
(30, '2022_02_23_071117_rename_graduation_certificate_in_employeeprofesionaldetail_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `usertype`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Demo', 'demo@email.com', NULL, '$2y$10$DNvzGpQ62Soqnxh540SuZ.8/gf/75uebUdn8BYoCzouA356yB5Zfe', 'admin', 'active', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD UNIQUE KEY `department_department_id_unique` (`department_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD UNIQUE KEY `employee_email_unique` (`email`),
  ADD UNIQUE KEY `employee_employee_id_unique` (`employee_id`);

--
-- Indexes for table `employeecoordinator`
--
ALTER TABLE `employeecoordinator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employeecoordinator_id_unique` (`id`),
  ADD KEY `employeecoordinator_department_id_foreign` (`department_id`),
  ADD KEY `employeecoordinator_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employeeprofessionaldetail`
--
ALTER TABLE `employeeprofessionaldetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employeeprofessionaldetail_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employeecoordinator`
--
ALTER TABLE `employeecoordinator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `employeeprofessionaldetail`
--
ALTER TABLE `employeeprofessionaldetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employeecoordinator`
--
ALTER TABLE `employeecoordinator`
  ADD CONSTRAINT `employeecoordinator_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employeecoordinator_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE;

--
-- Constraints for table `employeeprofessionaldetail`
--
ALTER TABLE `employeeprofessionaldetail`
  ADD CONSTRAINT `employeeprofessionaldetail_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
