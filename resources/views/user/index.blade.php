@extends('template.master')
@section('content')
    
    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{url('dashboard/department')}}">Department</a></li>
            </ol>
        </div>
    </div>
            

    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <h3 align="center">Department Details</h3><br />
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success" id="alert">
                                            <strong>Success:</strong> {{Session::get('success')}}
                                        </div>
                                    @elseif(session('error'))
                                        <div class="alert alert-danger" id="alert">
                                            <strong>Error:</strong>{{Session::get('error')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <span><a href="{{url('dashboard/department/create')}}"><button type="submit" class="btn btn-primary">Add</button></a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" name="search" id="search" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>User</th>
                                        <th>Email</th>
                                        <th>Usertype</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     @if(isset($list))
                                       @foreach($list as $value)
                                   <tr>
                                       <td></td>
                                       <td>{{$value->name}}</td>
                                       <td>{{$value->email}}</td>
                                       <td>{{$value->usertype}}</td>
                                   </tr>
                                    @endforeach
                                   @endif
                                </tbody>
                            </table>
                            
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>


@stop


