@extends('template.master')
@section('content')
    
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                    </ol>
                </div>
            </div>
            

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                            
                              <h3 align="center">Add Or Edit Form Update</h3><br />
                                <div class="form-validation">
                                    @if(@isset($errors))
                                        @foreach($errors->all() as $error)
                                            <div style="color:red;">
                                            {{$error}}
                                            </div>
                                        @endforeach
                                    @endif
                                    <form class="form-valide" action="{{url('/dashboard/department/edit')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" value="{{ isset($getdetail[0]->department_id)? Crypt::encrypt($getdetail[0]->department_id) : ' ' }}" name="department_id" id="dept_id">

                                        <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="Department-name"> Image<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                           @php
                                            if(isset($getdetail[0]->image)){
                                            if(($getdetail[0]->image =='')||($getdetail[0]->image ==NULL)){ @endphp
                                                <img id="preview-image-before-upload" src="#"
                                                alt="preview image" style="max-height: 250px;">
                                                
                                            @php }else{ @endphp
                                                <img id="preview-image-before-upload" src="{{asset('assets/images/public/'.$getdetail[0]->image)}}"
                                                alt="preview image" style="max-height: 250px;">
                                                        
                                            @php   } }else{ @endphp
                                                <img id="preview-image-before-upload" src="#"
                                                alt="preview image" style="max-height: 250px;">
                                            @php  }@endphp
                                            
                                                <input id="imageactivate" type="file" name="image" placeholder="Choose image">
                                                @if ($errors->has('image'))
                                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Department Name<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="departmentname" name="department_name" value="{{ isset($getdetail[0]->department_name)? $getdetail[0]->department_name : ' ' }}" placeholder="Enter a Department name..">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">Description <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control" id="Description" name="description" rows="5" placeholder="What would you like to see?">{{ isset($getdetail[0]->description)?$getdetail[0]->description: 'here ' }}</textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="total_numbers">total_numbers <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="number" class="form-control" id="total_numbers" name="total_members" value="{{ isset($getdetail[0]->total_members)? $getdetail[0]->total_members : ' ' }}" placeholder="0">
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button id="save" class="btn btn-success btn-rounded"  type="submit">Save</button>
                                                <button type="clear" class="btn btn-primary">Clear</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
   
                            </div>  
                              

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end

            ***********************************-->  
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>

     $(document).ready(function (e) {
        $('#imageactivate').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => { 
                                        $('#preview-image-before-upload').attr('src', e.target.result); 
                                    }
            reader.readAsDataURL(this.files[0]); 
        });
    });
 
    var dept_name = '';
    var dept_des = '';
    var dept_mem = '';
    var dept_id = $("#dept_id").val();
    if(dept_id == ''){
       dept_name = false;
     dept_des = false;
     dept_mem = false;
    }else{
       dept_name = true;
        dept_des = true;
      dept_mem = true;
    }
    
    function imagecheck(){
        var img='';
        var image = $('#imageactivate').val();
        if(image == ''){
            response('imageactivate','imageerror',"Insert Image");
            img = false;
        }else{
            var myImg = this.files[0];
            var myImgType = myImg["type"];
            var validImgTypes = ["image/gif", "image/jpeg", "image/png"];
            
            if ($.inArray(myImgType, validImgTypes) < 0) {
                response('imageactivate','imageerror',"Not An Image");
                img=false;
            }else{
                img = true;
            }
        }
        return img;
    }
 
    $(document).on('keyup', '#departmentname', function(){
        var deptname = $("#departmentname").val();
         var regEx =  /^[a-zA-Z\s]*$/;
        if(deptname == ''){
              response('departmentname','deptnameerror',"Department Name Cannot Be Blank");
              dept_name = false;
        }else{
            if(regEx.test(deptname))
            {
                
                $("#deptnameerror").remove();
                dept_name = true;
            }
            else
            {
                
                response('departmentname','deptnameerror',"Please enter letters and space only.");
                dept_name = false;
           
            }
        } 
    });

     $(document).on('keyup', '#Description', function(){
        var description = $("#Description").val();
     
        if(description == ''){
              response('Description','descriptionerror',"Description Name Cannot Be Blank");
              dept_des = false;
        }else{
            $("#descriptionerror").remove();
            dept_des = true;
        } 
    });

    $(document).on('keyup', '#total_numbers', function(){
        var total_numbers = $("#total_numbers").val();
        var numbers = /^[0-9]+$/;
        if(total_numbers == ''){
            
              response('total_numbers','totalnumbererror',"Total Numbers Name Cannot Be Blank Or 0");
              dept_mem = false;
        }else{
            
            if(isNaN(Number(total_numbers)))
            {
                response('total_numbers','totalnumbererror',"Please input numeric characters only");
                dept_mem = false;
                
            }
            else
            {
                $("#totalnumbererror").remove();
                dept_mem = true;
            }
        } 
    });

    function response(id,errorid,msg){
        
        if ($('#'+errorid).length)
        {
        /* it exists */
            $("#"+errorid).remove();
             $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }
        else
        {
        /* it doesn't exist */
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        
        }
    }

    if((dept_mem == true)&&(dept_des == true)&&(dept_name == true)){
      $('#save').removeAttr('disabled'); 
    }else{
      $('#save').prop('disabled', true);
    }
</script>

@stop


