-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2022 at 06:37 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `employeecoordinator`
--

CREATE TABLE `employeecoordinator` (
  `id` int(11) NOT NULL,
  `department_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `supervisor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employeecoordinator`
--

INSERT INTO `employeecoordinator` (`id`, `department_id`, `employee_id`, `created_at`, `updated_at`, `supervisor`) VALUES
(8, 'f_1', '1a5e2365-57e1-46f6-a4a8-47649bd217b1', NULL, '2022-02-15 08:14:40', 'active'),
(9, 'f_1', 'e58dec16-6e9f-406f-b166-91aa8e172a1b', NULL, NULL, NULL),
(10, 'f_1', '2980d8b3-964c-494a-920c-105f1b8e62cc', NULL, NULL, NULL),
(11, 'hr_1', '4e4520b0-e79c-40d1-a882-451c059f99db', NULL, '2022-02-15 08:16:11', 'active'),
(12, 'hr_1', '89acba24-47a0-4ab3-ad92-28474b2ff14d', NULL, '2022-02-15 08:16:11', 'active'),
(13, 'hr_1', '4ed63716-c27c-4f42-a5f6-2764bb26e392', NULL, NULL, NULL),
(14, 'hr_1', '960cb2cd-a664-4dbc-8b0b-7c9a4a83aff2', NULL, NULL, NULL),
(15, 'hr_1', '3e2cf3df-2481-401f-bd67-f7c7a4f8067f', NULL, NULL, NULL),
(16, 'hr_1', '3173fc54-b125-4549-9d51-0a66dab1465c', NULL, NULL, NULL),
(17, 'pl_1', '444f4c5f-9e9f-498b-b1a8-c0faf11bee93', NULL, '2022-02-15 08:25:13', 'active'),
(18, 'pl_1', '4b28c6c1-629b-49ee-a1a1-7df8bc309c76', NULL, '2022-02-15 08:25:13', 'active'),
(19, 'pl_1', '56d93300-0441-41d7-8666-e74bdf8bb917', NULL, '2022-02-15 08:25:13', 'active'),
(20, 'pl_1', 'cd7be9e2-8a4d-4221-a6d1-c53acbd2984b', NULL, '2022-02-15 08:25:13', 'active'),
(21, 'pl_1', '74f2f9d7-f752-4629-a61a-47501ca30b2f', NULL, '2022-02-15 08:25:13', 'active'),
(22, 'pl_1', '3250fc80-13de-4ea1-af8c-6b67568569aa', NULL, '2022-02-15 08:25:13', 'active'),
(23, 'pl_1', 'f7b9c428-6520-4f30-b192-5f24a86eaaa8', NULL, '2022-02-15 08:25:13', 'active'),
(24, 'pl_1', '03643192-fbf3-419d-8f01-01ba8737756e', NULL, '2022-02-15 08:25:13', 'active'),
(25, 'pl_1', '459cfe38-5d56-4ef7-98fd-2188a67b2464', NULL, '2022-02-15 08:25:13', 'active'),
(26, 'pm_1', '694ed023-23dc-4acd-9f18-990bf7461162', NULL, '2022-02-15 08:27:41', 'active'),
(27, 'pm_1', '4f58fa29-9651-46bd-a664-7f17536ca402', NULL, '2022-02-15 08:27:42', 'active'),
(28, 'pm_1', '195b408b-d6dc-4958-a725-446e0a0c2835', NULL, '2022-02-15 08:27:42', 'active'),
(29, 'qa_1', '09024e42-0774-4422-8f32-38e2397ae96a', NULL, NULL, NULL),
(30, 'qa_1', '28e105da-cc7a-4724-bf63-58c34c57055b', NULL, NULL, NULL),
(31, 'qa_1', 'e75e0a6a-ecc9-4adb-ba28-68efa6bbfa54', NULL, NULL, NULL),
(32, 'qa_1', 'd18693ee-f4b6-445c-b4e3-ff48b5b238c3', NULL, NULL, NULL),
(33, 'qa_1', '3a2eb97d-74a3-4fd1-b5ae-b1fa433f924c', NULL, NULL, NULL),
(34, 'qa_1', 'ea466963-766b-4f64-9f43-36eba112c512', NULL, NULL, NULL),
(35, 'ra_1', 'c6e2f60a-9323-4ed7-ad91-ef23390798e5', NULL, '2022-02-15 08:29:55', 'active'),
(36, 'ra_1', '7947eda8-4f9d-48f3-b302-de88f15f222f', NULL, '2022-02-15 08:29:55', 'active'),
(37, 'ra_1', '049e40b8-a4bb-4eaa-ae8e-8ebe61b79dcc', NULL, '2022-02-15 08:29:55', 'active'),
(38, 'ra_1', '08d15a32-f8c7-4e00-a635-d2d4ba228891', NULL, NULL, NULL),
(39, 'ra_1', '9841a4e2-6957-4d0e-a151-28759ac2ef65', NULL, NULL, NULL),
(40, 'ra_1', '4b989e59-a552-4c1a-855e-8940379426f0', NULL, NULL, NULL),
(41, 'sa_1', 'de3682e2-411e-46dd-baf2-24ea61f0a426', NULL, '2022-02-15 08:30:21', 'active'),
(42, 'sa_1', '28d1f803-9eba-474d-b36f-5eb757aee4a2', NULL, '2022-02-15 08:30:21', 'active'),
(43, 'sa_1', 'd01905fb-f402-4247-b355-e73bf7b727b0', NULL, '2022-02-15 08:30:21', 'active'),
(44, 'sa_1', '963369b3-16c3-433c-b459-e4b730768152', NULL, NULL, NULL),
(45, 'sa_1', '2a5a7db5-5b04-42cb-8dfa-58ab8eb5196b', NULL, NULL, NULL),
(46, 'sa_1', '6b3ed824-971b-4e89-b0f2-db8a03080c29', NULL, NULL, NULL),
(47, 'ui_1', '49834304-b60e-42d2-b9e9-cbd6be460dfd', NULL, '2022-02-15 08:31:07', 'active'),
(48, 'ui_1', '612a864d-4b04-49cf-ac96-eaa3b2761e6b', NULL, '2022-02-15 08:31:07', 'active'),
(49, 'ui_1', '88d6cf1c-ba7f-40ce-9cdf-07756ecd0284', NULL, NULL, NULL),
(50, 'ui_1', '08aaedd5-d61d-4314-ab15-c8cbff6fb74d', NULL, NULL, NULL),
(51, 'ui_1', 'd7d79d34-a2c9-4530-944c-6ae80912d8f4', NULL, NULL, NULL),
(52, 'ui_1', '4ba2647f-55dd-44bc-b8ed-e2039214b508', NULL, NULL, NULL),
(53, 'wd_1', '521706e4-911a-437f-90d5-fb00f0418574', NULL, NULL, NULL),
(54, 'wd_1', '1e6593a8-97dc-446b-80c1-8c586dce98a5', NULL, NULL, NULL),
(55, 'wd_1', '4986cc8a-d9db-443d-8540-7f95589ba1fe', NULL, NULL, NULL),
(56, 'wd_1', '9807ddbd-1de4-45b8-b9c5-9d0c09046bd6', NULL, NULL, NULL),
(57, 'wd_1', 'b46af98a-b83a-48d5-802c-03283530b80a', NULL, NULL, NULL),
(58, 'wd_1', 'a744ebba-1573-45df-ae3b-773c3899ae46', NULL, NULL, NULL),
(59, 'wd_1', '9eb34bed-1820-4b3a-aea3-eacf09bab876', NULL, NULL, NULL),
(60, 'wd_1', 'a0da0409-f23d-4093-931d-a0aa58369673', NULL, NULL, NULL),
(61, 'wd_1', '15d96efb-1031-489a-bc69-2a6b31ef9318', NULL, NULL, NULL),
(62, 'wd_1', 'ea23aea7-57c3-42ed-a5e5-ae92229e9bda', NULL, NULL, NULL),
(63, 'wd_1', '4062d341-e917-40d9-a7bd-682bc8fe4df5', NULL, NULL, NULL),
(64, 'wd_1', '85055755-3d88-4614-aba6-53367b247be6', NULL, NULL, NULL),
(65, 'wd_1', '003943a6-eae5-43f4-bf19-3c71606c1e64', NULL, NULL, NULL),
(66, 'wd_1', 'fff4f545-2ee5-4475-9823-38e906e0bc56', NULL, NULL, NULL),
(67, 'wd_1', 'ed28918f-5e87-4572-98d9-35cacd6cff96', NULL, NULL, NULL),
(68, 'wd_1', 'b5ade706-d61d-4068-b1fb-9c83c30ac2f5', NULL, NULL, NULL),
(69, 'wd_1', '97caf874-5c86-4b67-a5a7-b1ef24c4a33a', NULL, NULL, NULL),
(70, 'wd_1', 'a7292e76-8feb-4c35-b01b-69a0da102f7b', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employeecoordinator`
--
ALTER TABLE `employeecoordinator`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employeecoordinator_id_unique` (`id`),
  ADD KEY `employeecoordinator_department_id_foreign` (`department_id`),
  ADD KEY `employeecoordinator_employee_id_foreign` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employeecoordinator`
--
ALTER TABLE `employeecoordinator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employeecoordinator`
--
ALTER TABLE `employeecoordinator`
  ADD CONSTRAINT `employeecoordinator_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employeecoordinator_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
