<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {

            $table->string('employee_id',255)->unique();
            $table->string('image',255)->nullable();
            $table->string('employee_name',50);
            $table->string('gender',30)->nullable();
            $table->integer('employee_phone_no',12)->nullable();
            $table->string('email',20)->nullable();
            $table->string('address',500)->nullable();
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
