@extends('template.master')
@section('content')
    
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{url('dashboard/memberinfo')}}">Memberlist</a></li>
                        <li class="breadcrumb-item active"><a href="javascript:void(0);">add/edit_form</a></li>
                    </ol>
                </div>
            </div>
            

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                            
                              <h3 align="center">Add Or Edit Form Update</h3><br />
                                <div class="form-validation">
                                    @if(@isset($errors))
                                        @foreach($errors->all() as $error)
                                            <div style="color:red;">
                                            {{$error}}
                                            </div>
                                        @endforeach
                                    @endif
                                    <form class="form-valide" action="{{url('/dashboard/membersinfo/edit')}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ isset($getdetail[0]->employee_id)? Crypt::encrypt($getdetail[0]->employee_id) : ' ' }}" name="employee_id" id="dept_id">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Name<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_name" name="employee_name" value="{{ isset($getdetail[0]->employee_name)? $getdetail[0]->employee_name : ' ' }}" placeholder="Enter a Employee name..">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Deptment Id<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_dept_id" name="employee_dept_id" value="{{ isset($getdetail[0]->employee_dept_id)? $getdetail[0]->employee_dept_id : ' ' }}" placeholder="Enter a Employee Department Id name..">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Phone<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_phone_no" name="employee_phone_no" value="{{ isset($getdetail[0]->employee_phone_no)? $getdetail[0]->employee_phone_no : ' ' }}" placeholder="Enter a Employee Phone Number..">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Email<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="email" name="email" value="{{ isset($getdetail[0]->email)? $getdetail[0]->email : ' ' }}" placeholder="Enter a Employee Email..">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">Address <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control" id="address" name="address" rows="5" placeholder="What would you like to see?">{{ isset($getdetail[0]->address)?$getdetail[0]->address: ' ' }}</textarea>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button id="save" class="btn btn-success btn-rounded"  type="submit">Save</button>
                                                <button type="clear" class="btn btn-primary">Clear</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
   
                            </div>  
                              

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end

            ***********************************-->  
<!-- <script>
    var dept_name = '';
    var dept_des = '';
    var dept_mem = '';
    var dept_id = $("#dept_id").val();
    if(dept_id == ''){
       dept_name = false;
     dept_des = false;
     dept_mem = false;
    }else{
       dept_name = true;
        dept_des = true;
      dept_mem = true;
    }
    
    
 
    $(document).on('keyup', '#departmentname', function(){
        var deptname = $("#departmentname").val();
         var regEx =  /^[a-zA-Z\s]*$/;
        if(deptname == ''){
              response('departmentname','deptnameerror',"Department Name Cannot Be Blank");
              dept_name = false;
        }else{
            if(regEx.test(deptname))
            {
                
                $("#deptnameerror").remove();
                dept_name = true;
            }
            else
            {
                
                response('departmentname','deptnameerror',"Please enter letters and space only.");
                dept_name = false;
           
            }
        } 
    });

     $(document).on('keyup', '#Description', function(){
        var description = $("#Description").val();
     
        if(description == ''){
              response('Description','descriptionerror',"Description Name Cannot Be Blank");
              dept_des = false;
        }else{
            $("#descriptionerror").remove();
            dept_des = true;
        } 
    });

    $(document).on('keyup', '#total_numbers', function(){
        var total_numbers = $("#total_numbers").val();
        var numbers = /^[0-9]+$/;
        if(total_numbers == ''){
            
              response('total_numbers','totalnumbererror',"Total Numbers Name Cannot Be Blank Or 0");
              dept_mem = false;
        }else{
            
            if(isNaN(Number(total_numbers)))
            {
                response('total_numbers','totalnumbererror',"Please input numeric characters only");
                dept_mem = false;
                
            }
            else
            {
                $("#totalnumbererror").remove();
                dept_mem = true;
            }
        } 
    });

    function response(id,errorid,msg){
        
        if ($('#'+errorid).length)
        {
        /* it exists */
            $("#"+errorid).remove();
             $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }
        else
        {
        /* it doesn't exist */
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        
        }
    }

    if((dept_mem == true)&&(dept_des == true)&&(dept_name == true)){
      $('#save').removeAttr('disabled'); 
    }else{
      $('#save').prop('disabled', true);
    }
</script> -->

@stop


