@extends('template.master')
@section('content')
    
            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{url('dashboard/memberinfo')}}">Memberlist</a></li>
                        <li class="breadcrumb-item active"><a href="{{url('dashboard/memberinfo')}}">{{$department_type}} Department</a></li>
                    </ol>
                </div>
            </div>
            

            <div class="container-fluid">
                @php  
                    foreach($final_response as $key=>$data){
                @endphp   
                      <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                              @include('employeelist.depatmentinfo');
                              

                            </div>
                        </div>
                    </div>
                </div> 
                   @php
                    }
                   @endphp
               
                <!-- <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">

                              

                            </div>
                        </div>
                    </div>
                </div> -->

            </div>
            <!-- #/ container -->
        </div>
    <!--**********************************
            Content body end

            ***********************************-->  
            
<script>
    $(document).ready(function(){

    function clear_icon()
    {
        $('#employee_name').html('');
        $('#designation').html('');
        $('#email').html('');
    }

    function fetch_data(page, sort_type, sort_by, query)
    {
       var str = sort_by.split("-");
       
    var url ="{{url('dashboard/employeeList/fetchdata')}}";
    $.ajax({

    type: "POST",

    data: {"_token": "{{ csrf_token() }}","page":page,"sort_type":sort_type,"sort_by":str[0],"search":query,"dept":str[1] },

    url: url,
    success: function(msg){
        $('#'+str[1]).html('');
        if(msg =='data not available'){
           var msg= '<td colspan="5" align="center">'+msg+'</td>';
           $('#'+str[1]).after(msg);
        }else{
         console.log(msg);   
        var data =msg.data;
        var i=0;
        for(i=0;i<data.length;i++){

        var id = data[i]['employee_id'];
        var employee_id = id;//encrypt(id);
        var response = '<tr><td>'+data[i]['employee_name']+'</td><td>'+data[i]['designation']+'</td><td>'+data[i]['email']+'</td><td><span><a href="{{url("dashboard/employeelist/update",'+id+')}}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a></td></tr>';

        $('#'+str[1]).after(response);
     }
     var links = '<td colspan="5" align="center">'+msg['links']("pagination::bootstrap-4")+'</td>';
     $('#'+str[1]).after(links);
        }
      
     }
    });
}

    $(document).on('keyup', '#search', function(){
    var query = $('#search').val();
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();
    var page = $('#hidden_page').val();

    fetch_data(page, sort_type, column_name, query);
    });

    $(document).on('click', '.sorting', function(){
        alert('cfg');
    var column_name = $(this).data('column_name');
    var order_type = $(this).data('sorting_type');
    var reverse_order = '';
    if(order_type == 'asc')
    {
    $(this).data('sorting_type', 'desc');
    reverse_order = 'desc';
    clear_icon();
    $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-bottom"></span>');
    }
    if(order_type == 'desc')
    {
    $(this).data('sorting_type', 'asc');
    reverse_order = 'asc';
    clear_icon
    $('#'+column_name+'_icon').html('<span class="glyphicon glyphicon-triangle-top"></span>');
    }
    $('#hidden_column_name').val(column_name);
    $('#hidden_sort_type').val(reverse_order);
    var page = $('#hidden_page').val();
    var query = $('#search').val();
   
    fetch_data(page, reverse_order, column_name, query);
    });

    $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();

    var query = $('#search').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, sort_type, column_name, query);
    });

    });
</script>
@stop


