<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Session;
use DateTime;

class UserController extends Controller
{
  
    public function index(){
      $list = '';
      $list = User::getlist();
      return view('user.index',compact('list'));
    }

    public function userregister(){
      return view('user.register');
    } 

    public function userlogin(){
      return view('user.login');
    }
    
     public function LoginAttempt(Request $request){
      $attempt = 0;
      $error = '';
     $now = date('Y-m-d H:i:s'); 
      $checkuserexist = User::checkuserexist($request->input('email'));
     
      if($checkuserexist->isEmpty()){
        $error = "registered user are permitted to login"; 
      }else{ 
        
        if(($checkuserexist[0]->usertype =='admin')||($checkuserexist[0]->usertype =='hr')||($checkuserexist[0]->usertype =='pm')){
         
            $checkuserattempt = User::checkuserattempt($request->input('email')); //check user attempt
           
            if ($checkuserattempt) {
              //user exist
              if (is_null($checkuserattempt[0]->login_attempt_date)) {
                  //new user
                  $attempt =$checkuserattempt[0]->attempt+1;
                  goto end;
              } else {
                  //old user
                  if (date($checkuserattempt[0]->login_attempt_date)==date('Y-m-d')) {
                      //logged in today
                      $date1 = new DateTime($checkuserattempt[0]->login_attempt_date);
                      $date2 = new DateTime($now);
                      $difference = $date1->diff($date2);
                      $diffInHours   = $difference->h;
           
                      if ($diffInHours<=24) {
                          //within 24 hrs
                          if ($checkuserattempt<3) {
                              //user can attempt to login
                              $attempt =$checkuserattempt[0]->attempt+1;
                              goto end;
                          } else {
                              //user not allowed to attempt
                              $error = "You Already tried 3 times.your account is suspended for 24 hours.";
                          }
                      } else {
                          //24 hrs end
                          $attempt =1;
                          goto end;
                      }
                  } else {
                      //old user logged in after many days
                      $attempt =1;
                      goto end;
                  }
              }
              end:
       if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
           //auth check successs
           $user = Auth::User();
            $value = [
                        'user_id'=>$user->id,
                           'username'=>$user->name,
                           'email'=>$user->email,
                           'usertype'=>$user->usertype
            ];
                
            Session::put('userinfo', $value);               
            return redirect('/dashboard');
        }else{
            //auth check not success
            $userattemptrecord = User::attemptrecord($request->input('email'), $now, $attempt); //update user attempt
            $left= 3-$attempt; //chances user have
            if($left ==0){
              $error = "You Already tried 3 times.your account is suspended for 24 hours.";
            }else{
                $error = 'The provided credentials do not match our records. '.$left.' attempts left. Please try again';
            }
            
        }
          }else{
            $error = "registered user are permitted to login";
          }
          } else {
              //user doesnot exist
              $error = "registered user are permitted to login";
          }
      }
       
        if(!empty($error)){
          return back()->withErrors([
            'error' =>$error,
        ]);
        }
         
    }

    public function logout(){
      //logout code here
      Auth::logout();
      Session::flush();
      return redirect('/login');
    }
    
}
