<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        // 
        // for($i=0;$i<50;$i++){
        //   DB::table('employee')->insert([
        //     'employee_id' => Str::random(10), 
        //     'employee_name' => Str::random(10),
        //     'employee_dept_id'=> Str::random(5),
        //     'email' => Str::random(10).'@gmail.com',
        //     'employee_phone_no'=>mt_rand( 10000000, 99999999 ),
        // ]);
        // }
        $data = 
            [
                [
                    [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'James Nazareth',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"9088888888",
                        'email'=> 'JamesNazaretha@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Benjamin Noronha',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"9077777777",
                        'email'=> 'BenjaminNoronha@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                    [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Lucas Oliveira',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"90666666666",
                        'email'=> 'LucasOliveira@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Henry Ouseph',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"90555555555",
                        'email'=> 'HenryOuseph@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Alex Pathrose',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"90444444444",
                        'email'=> 'AlexPathrose@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Emma Paulose',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"90333333333",
                        'email'=> 'EmmaPaulose@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Ava Peeri',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"90222222222",
                        'email'=> 'AvaPeeri@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                    [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Charlotte Pereira',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"90111111111",
                        'email'=> 'CharlottePereira@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Sophia Pinto',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"9008888888",
                        'email'=> 'SophiaPinto@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Amelia Quadros',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"9009999999",
                        'email'=> 'AmeliaQuadroso@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                     [ 
                        'employee_id'=>Str::uuid()->toString(),
                        'employee_name'=>'Mia Rebelo',
                        'employee_dept_id' => 'wd_1',
                        'employee_phone_no'=>"9007777777",
                        'email'=> 'MiaRebelo@gmail.com',
                        'address' => 'Los Angeles, California, United Kingdom.',
                    ],
                    
                ]
            ];

        foreach ($data as $keyd=>$valued) {
             
                    DB::table('employee')->insert($valued);
                    $this->command->info($keyd);//gives you an idea where your iterator is in command line, best feeling in the world to see it rising if you ask me :D
                }
          
    }
}
