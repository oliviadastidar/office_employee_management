 @if(isset($list))
    @foreach($list as $key=>$data)
      @php $id= Crypt::encrypt($data->id);
       @endphp
                 <!--Encrypt ID and store as $prodID-->
        <tr>
            <td>{{$data->id}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->detail}}</td>
            <td>{{$data->status}}</td>
            
            <td> @if(request()->has('trashed'))
                <a href="{{ route('qualification.restore', $id) }}" class="btn btn-success">Restore</a>
            @else
                <form method="POST" action="{{ route('qualification.destroy', $id) }}">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <button type="submit" class="btn btn-danger delete" title='Delete'><i class="fa fa-close color-danger"></i></button>
                </form>
            @endif<span><a href="{{url('dashboard/qualification/update',$id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil color-muted m-r-5"></i> </a></span></td>
        </tr>
    @endforeach
    <tr>
     
@endif
                                       