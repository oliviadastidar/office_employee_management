<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeProfessionalDetail extends Model
{
    use HasFactory;

    protected $table = 'employeeprofessionaldetail';
    
    protected $fillable = [
                            'employee_id',
                            'designation',
                            'experience_detail',
                            'qualification',
                            'technical_skills',
                            'release_letter',
                            'experience_letter',
                            'payslip',
                            'joiningdate',	
                            'experience_in_previous_company',
                            'experience_in_our_company',
    ];
    public static function getemployeeprofessionaldetails($id){
      
        $response = EmployeeProfessionalDetail::where('employee_id','=',$id)->get();
        
        return $response;
    }
   
    public static function scopeUpdateemployee($id,$array){
        
        $response = EmployeeProfessionalDetail::where('employee_id', $id)  // find your user by their email
        ->update($array); 
       
        return $response;
    }

    public static function addemloyee($uuid){
        $response = EmployeeProfessionalDetail::insert(['employee_id'=>$uuid]);
        return $response;
    }

}
