<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeprofessionaldetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employeeprofessionaldetail', function (Blueprint $table) {
            $table->integer("id")->unique();
            $table->string("employee_id",255)->nullable();
            $table->string("designation",255)->nullable();
            $table->string("experience_detail",255)->nullable();
            $table->integer("qualification",)->nullable();
            $table->integer("experience_in_previous_company",255)->nullable();
            $table->integer("experience_in_our_company",255)->nullable();
            
            $table->string("technical_skills",255)->nullable();
            $table->string("release_letter",255)->nullable();
            $table->string("experience_letter",255)->nullable();
            $table->string("payslip",255)->nullable();
            $table->string("joiningdate",255)->nullable();
            $table->softDeletes();
            $table->timestamps();
            //$table->foreign('employee_id')->references('employee_id')->on('employee')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employeeprofessionaldetail');
    }
}
