<?php

namespace App\Models;
use App\Models\Deppartment;
use App\Models\EmployeeProfessionalDetail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
class EmployeeCoordinator extends Model
{
    use HasFactory;
    
    protected $table = "employeecoordinator";
    
    protected $fillable = [
                            'employee_id',
                            'department_id',
                            'supervisor'
                          ];
    
    public static function Getdeptemployeelist($id){
   
        $response = EmployeeCoordinator::select('employee_id')->where('department_id',$id)->orderby('id','asc');//->paginate(5);
        
        return $response;
    }
    public static function add($arr){
        $response = EmployeeCoordinator::insert($arr);
        return $response;
    }
    

}
