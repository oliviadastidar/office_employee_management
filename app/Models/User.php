<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'usertype',
        'status'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public static function checkuserexist($email){
        $response = User::select('usertype')->where('email',$email)
                           ->get();
        return $response;                   
    }
     public static function checkuserattempt($email){
       $data = User::select('attempt', 'login_attempt_date')
                           ->where('email', $email)
                           ->get();
        
        return $data;
    }

    public static function attemptrecord($email,$date,$attempt){
        
        $update = User::where('email', $email)  // find your user by their email
                        ->limit(1)  // optional - to ensure only one record is updated.
                        ->update(array('attempt' => $attempt,'login_attempt_date'=>$date));  
        if($update){
            return 1;
        }else{
            return 0;
        }                
    }

    public static function getlist(){
        $response = User::all('name','email','usertype');
        return $response;
    }
}
