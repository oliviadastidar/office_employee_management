<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Department extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'department';
    
    protected $fillable = [ 
                            'department_name',
                            'description',
                            'total_members'
    ];

    protected $gaurded = ['department_id'];

    public static function getthrashed(){
        $response = Department::onlyTrashed()
                ->get();

                return $response;
    }
    public static function getdepartmentlist(){
        $list = Department::paginate(5);
        return $list;
    }

     public static function filterdata($sort_by,$sort_type,$query){
        $list = Department::where('department_id', 'like', '%'.$query.'%')
                    ->orWhere('department_name', 'like', '%'.$query.'%')
                    ->orWhere('total_members', 'like', '%'.$query.'%')
                    ->orderBy($sort_by, $sort_type)
                    ->paginate(5);
        return $list;
    }

    public static function getiddetail($id){
        
        $detail = Department::select("*")
                        ->where("department_id", $id)
                        ->get();
        
        return $detail;
    }

    public static function updatedepartment($id,$data,$image){
    
        $array = [
            'image' => $image,
            'department_name'=> $data['department_name'],
            'description' => $data['description'],
            'total_members' => $data['total_members'],
        ];
        
        $response = Department::updateOrInsert(
        ['department_id' => $id],
        $array
    );
        return $response;

    }

    public static function getdata(){
        $response = Department::all('department_id','total_members');
        
        return $response;
    }
    
    public static function getdepartmentdetail($value){
        $response = Department::all('department_id','total_members')->whereIn('department_id',$value);
        
        return $response;
    }
    public static function scopeGetalldata($dept_Array){
        foreach ($dept_Array as $key => $value) {
            # code...
            $response[] = Department::where('department_id',$value)->get();
        }
    
        return $response;
    }

    public static function getdepartmentname($id){
       
        $response = Department::where('department_id','=',$id[0]['departement_id'])->get('department_name');
       
        return $response;
    }

    public static function getlist(){
        $response = Department::all('department_id','department_name');
        return $response;
    }

    public static function destroy($id){
        $response =Department::where('department_id',$id)->delete();
        return $response;
    }
    public static function restoreid($id){
        $restore = Department::withTrashed()->find($id)->restore();
        return $restore;
    }
    public static function restoreall(){
        $restore = Department::onlyTrashed()->restore();
         return $restore;
    }
}
