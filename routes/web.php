<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\MemberListController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeListController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\QualificationController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/register',[UserController::class,'userregister']);
Route::get('/login',[UserController::class,'userlogin'])->middleware('alreadyLoggedIn');
Route::get('/logout',[UserController::class,'logout']);
Route::post('/loginattempt',[UserController::class,'loginattempt'])->middleware('alreadyLoggedIn');

Route::prefix('/dashboard')->group(function () {
  Route::get('/',[DashboardController::class,'index'])->middleware('alreadyLoggedIn');

Route::prefix('/employee')->group(function () {
      Route::get('/', [EmployeeController::class,'index'])->middleware('alreadyLoggedIn');
      Route::get('/create',[EmployeeController::class,'create'])->middleware('alreadyLoggedIn');
      Route::post('/store',[EmployeeController::class,'store'])->middleware('alreadyLoggedIn');
      Route::post('/fetchdata', [EmployeeController::class,'getfilterdata'])->middleware('alreadyLoggedIn');
      Route::get('/update/{employee_id}',[EmployeeController::class,'edit'])->middleware('alreadyLoggedIn');
      Route::delete('/{employee_id}', [EmployeeController::class, 'destroy'])->name('employee.destroy');
      Route::get('/restore/{employee_id}', [EmployeeController::class, 'restore'])->name('employee.restore');
      Route::get('/restore-all', [EmployeeController::class, 'restoreAll'])->name('employee.restoreAll');
});

  Route::prefix('/employeelist')->group(function () {
      Route::get('/',[EmployeeListController::class,'index'])->middleware('alreadyLoggedIn');
      Route::get('/show/{department_type}',[EmployeeListController::class,'show'])->middleware('alreadyLoggedIn');
      Route::post('/edit',[EmployeeListController::class,'store'])->middleware('alreadyLoggedIn');
      Route::post('/fileupload',[EmployeeListController::class,'upload'])->middleware('alreadyLoggedIn');
      Route::post('/professionaldataedit',[EmployeeListController::class,'proffesionaldatastore'])->middleware('alreadyLoggedIn');
       Route::get('/update/{employee_id}',[EmployeeListController::class,'edit'])->middleware('alreadyLoggedIn');
        
  });

    Route::prefix('/department')->group(function () {
      Route::get('/',[DepartmentController::class,'index'])->middleware('alreadyLoggedIn');
      Route::get('/show/{value}',[DepartmentController::class,'index'])->middleware('alreadyLoggedIn');
      Route::post('/fetchdata',[DepartmentController::class,'getfilterdata'])->middleware('alreadyLoggedIn');
      Route::get('/create',[DepartmentController::class,'create'])->middleware('alreadyLoggedIn');
      Route::get('/update/{department_id}',[DepartmentController::class,'edit'])->middleware('alreadyLoggedIn');
      Route::post('/edit',[DepartmentController::class,'store'])->middleware('alreadyLoggedIn');
      Route::delete('/{department_id}', [DepartmentController::class, 'destroy'])->name('posts.destroy');
      Route::get('/restore/{department_id}', [DepartmentController::class, 'restore'])->name('posts.restore');
      Route::get('/restore-all', [DepartmentController::class, 'restoreAll'])->name('posts.restoreAll');
    });
    Route::prefix('/qualification')->group(function () {
      Route::get('/',[QualificationController::class,'index'])->middleware('alreadyLoggedIn');
      Route::get('/update/{id}',[QualificationController::class,'edit'])->middleware('alreadyLoggedIn');
      Route::get('/create',[QualificationController::class,'create'])->middleware('alreadyLoggedIn');
      Route::post('/edit',[QualificationController::class,'store'])->middleware('alreadyLoggedIn');
      Route::delete('/{id}', [QualificationController::class, 'destroy'])->name('qualification.destroy');
      Route::get('/restore/{id}', [QualificationController::class, 'restore'])->name('qualification.restore');
      Route::get('/restore-all', [QualificationController::class, 'restoreAll'])->name('qualification.restoreAll');
   }); 
   Route::prefix('/profile')->group(function () {
       Route::get('/', [UserController::class,'index'])->middleware('alreadyLoggedIn');
      //  Route::get('/update/{id}', [QualificationController::class,'edit']);
      //  Route::get('/create', [QualificationController::class,'create']);
      //  Route::post('/edit', [QualificationController::class,'store']);
   });
   Route::prefix('/user')->group(function () {
       Route::get('/logout', [UserController::class,'logout']);
      //  Route::get('/update/{id}', [QualificationController::class,'edit']);
      //  Route::get('/create', [QualificationController::class,'create']);
      //  Route::post('/edit', [QualificationController::class,'store']);
   });
});
