<?php

namespace App\Http\Controllers;
use Session;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Illuminate\Support\Str;

class MemberListController extends Controller
{
    //63members
    
    public function index(Request $request){
       
        $memberlist = [];
        $memberlist = Employee::getmemberlist();
        if($request->ajax){
            return view('memberlist.memberinfo',compact('memberlist'));
        }else{
            return view('memberlist.index',compact('memberlist'));
        }

    }

    public function getfilterdata(Request $request){
        if($request->ajax())
     {
      $sort_by = $request->input('sort_by');
      $sort_type = $request->input('sort_type');
            $query = $request->input('search');
            $query = str_replace(" ", "%", $query);
      $memberlist =Employee::filterdata($sort_by,$sort_type,$query);
     
    //    
      return view('memberlist.memberinfo', compact('memberlist'))->render();
     }
    }
    
    public function create(){
       
    
       return view('memberlist.add_edit');
    }

    
    public function edit($employee_id){
       $id = Crypt::decrypt($employee_id);
       $getdetail = Employee::getiddetail($id);
    
       return view('memberlist.add_edit',compact('getdetail'));
    }

    public function store(Request $request){
        $uuid = Str::uuid()->toString();
       // dd($uuid);
        //$id = Crypt::decrypt($request->employee_id);
        //dd($id);
        // $validator = validator()->make(request()->all(), [
        //                 'employee_name' => 'required|regex:/^[\pL\s\-]+$/u|unique:employee,employee_name,'.$id.',employee_id',
        //                 'description' => 'required',
        //                 'total_members' => 'required|integer',
        // ]);

        // if ($validator->fails())
        // {
        //   return back()->withErrors($validator)->withInput();
        // }else{
           //dd($request->all());
          // if($request->employee_id != ''){
            
          //  $response = Employee::updateemployee($uuid,$id,$request->all());   
          // }else{
            $response = Employee::updateemployee($uuid,0,$request->all());
          //}
        // }

        if($response){
          //$status = "Department updated successfully";
           return redirect(url('dashboard/membersinfo'))->with('success', 'Data Your Comment has been created successfully'); //route('dashboard')->with('success',"Department updated successfully"); 
           
        }else{
          return back()->withErrors('Something Went Wrong.Please Try Again Later');
        }
    }
}
