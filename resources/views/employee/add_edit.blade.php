@extends('template.master')
@section('content')
    
    <div class="row page-titles mx-0">
        <div class="col p-md-0">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{url('dashboard/emloyee')}}">Emploee List</a></li>
                <li class="breadcrumb-item active"><a href="{{url('dashboard/emloyee/create')}}">User Detail</a></li>
            </ol>
        </div>
    </div>
 
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                    <!--personal details  -->
                        <div><h3 align="center">User Personal Details</h3><br /> 
                            <span id="edit" style="position:absolute;right:10px;top:25px;" value="false"><i class="fa fa-pencil color-muted m-r-5"></i> </span></div>
                                
                                <form class="form-valide" action="{{url('/dashboard/employee/store')}}" id="personaldetail"  method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                                @csrf
                                          
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="Department-name">Employee Image<span class="text-danger">*</span>
                                        </label>
                                        <div class="col-lg-6">
                                           
                                                <img id="preview-image-before-upload" src="#"
                                                alt="preview image" style="max-height: 250px;">
                                            
                                                <input id="imageactivate" type="file" name="image" placeholder="Choose image">
                                                @if ($errors->has('image'))
                                                    <span class="text-danger">{{ $errors->first('image') }}</span>
                                                @endif
                                            </div>
                                        </div>
             
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Name<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_name" name="employee_name"  placeholder="Enter a Employee name..">
                                                 @if ($errors->has('employee_name'))
                                                    <span class="text-danger">{{ $errors->first('employee_name') }}</span>
                                                @endif
                                            </div>
                                        </div> 
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Gender<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6" id="gen">
                                                <input type="radio" id="gendermale" name="gender" value="male">
                                                 <label for="html">Male</label>
                                                <input type="radio" id="genderfemale" name="gender" value="female">
                                                <label for="html">Female</label>
                                            </div>
                                             @if ($errors->has('gender'))
                                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                                            @endif
                                        </div> 
                                         
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Employee Phone<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="employee_phone_no" name="employee_phone_no" placeholder="Enter a Employee Phone Number..">
                                                @if ($errors->has('employee_phone_no'))
                                                    <span class="text-danger">{{ $errors->first('employee_phone_no') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label" for="Department-name">Department Name<span class="text-danger">*</span>
                                                    </label>
                                                <div class="col-lg-6">
                                                    <select id="department_name" name="department_name" class="form-control form-control-lg">
                                                        <option value="">none</option> 
                                                        @foreach($department_list as $list) 
                                                        <option value="{{$list->department_name}}">{{$list->department_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Department-name">Email<span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="email" name="email"  placeholder="Enter a Employee Email..">
                                                 @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="Description">Address <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control" id="address" name="address" rows="5" placeholder="What would you like to see?"></textarea>
                                            </div>
                                             @if ($errors->has('address'))
                                                <span class="text-danger">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                       
                                        <div class="form-group row" id="personaldetailaction">
                                            <div class="col-lg-8 ml-auto">
                                                <button id="personaldetailsave" class="btn btn-success btn-rounded"  type="submit" >Save</button>
                                                <button type="reset" class="btn btn-primary">Clear</button>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div> 

                
            </div> 
        </div>
            <!-- #/ container -->
    </div>

        <!--**********************************
            Content body end

            ***********************************-->  
            
    <script>
     
    $(document).ready(function (e) {
        $('#imageactivate').change(function(){
            let reader = new FileReader();
            reader.onload = (e) => { 
                                        $('#preview-image-before-upload').attr('src', e.target.result); 
                                    }
            reader.readAsDataURL(this.files[0]); 
        });
    });
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script>
        var emp_name = '';
        var emp_ph_no = '';
        var emp_email = '';
        var emp_add = '';
        
        var e_id_per = $("#emp_per_id").val();
        if(e_id_per == ''){
            emp_name = false;
            emp_ph_no = false;
            emp_email = false;
            emp_add = false;
            
        }else{
            emp_name = true;
            emp_ph_no = true;
            emp_email = true;
            emp_add = true;
            
        }
    
    $(document).on('keyup', '#department_name', function(){
        departmentnamevalidate();
    });

    $(document).on('keyup', '#address', function(){
         addressvalidate();
    });

    $(document).on('keyup', '#employee_phone_no', function(){
        employeephonenumber();
    });
    
    $(document).on('keyup', '#email', function(){
         emailvalidate();
    });

    $('#personaldetail').submit(function (e) {
        if((dept_name == true)&&(emp_desig == true)&&(empexp_old == true)&&(empexpnew == true)&&(emp_quali == true)&&(emptech_skill == true)){
            $('#personaldetailsave').removeAttr('disabled'); 
        }else{
            $('#personaldetailsave').prop('disabled', true);
        }
    });

    function employeenamevalidate(){
        var empname = $("#employee_name").val();
        var regEx =  /^[a-zA-Z\s]*$/;
        if(empname == ''){
            response('employee_name','employeenameerror',"Employee Name Cannot Be Blank");
            emp_name = false;
        }else{
            if(regEx.test(empname)){
                $("#employeenameerror").remove();
                emp_name = true;
            }else{
                response('employee_name','employeenameerror',"Please enter letters and space only.");
                dept_name = false;
            }
        } 
        return emp_name;
    }

    function addressvalidate(){
        var address = $("#address").val();    
        if(address == ''){
            response('Address','addresserror',"Address Cannot Be Blank");
            emp_add = false;
        }else{
            $("#addresserror").remove();
            emp_add = true;
        } 
        return emp_add;
    }
    
    function employeephonenumber(){
        var ph_numbers = $("#employee_phone_no").val();
        var numbers = /^[0-9]+$/;
        if(ph_numbers == ''){
            response('Phone_numbers','phonenumbererror',"Phone Numbers Name Cannot Be Blank Or 0");
            emp_ph_no = false;
        }else{
            if(isNaN(Number(ph_numbers))){
                response('Phone_numbers','phonenumbererror',"Please input numeric characters only");
                emp_ph_no = false;  
            }else{
                $("#phonenumbererror").remove();
                emp_ph_no = true;
            }
        }
        return emp_ph_no; 
    }

    function emailvalidate(){
        var email = $("#email").val();
        if(email == ' '){
            response('Email','emailerror',"Email Cannot Be Blank");
            emp_email = false;  
        }else{
            let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (email.match(regexEmail)) {    
                emp_email= true; 
            } else {
                emp_email= false; 
            }
        }
        return emp_email;
    }

    $(document).on("change", "#imageactivate", function() {
        imagecheck();
    });
 
    function imagecheck(){
        var img='';
        var image = $('#imageactivate').val();
        if(image == ''){
            response('imageactivate','imageerror',"Insert Image");
            img = false;
        }else{
            var myImg = this.files[0];
            var myImgType = myImg["type"];
            var validImgTypes = ["image/gif", "image/jpeg", "image/png"];
            
            if ($.inArray(myImgType, validImgTypes) < 0) {
                response('imageactivate','imageerror',"Not An Image");
                img=false;
            }else{
                img = true;
            }
        }
        return img;
    }

    function response(id,errorid,msg){
        if ($('#'+errorid).length){
            /* it exists */
            $("#"+errorid).remove();
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }else{
            /* it doesn't exist */
            $("#"+id).after("<p id='"+errorid+"' style='color:red;'>"+msg+"</p>"); 
        }
    }
    
    function radiocheck(){
        var checkRadio = document.querySelector('input[name="gender"]:checked'); 
        var radiocheck ='';
        if (checkRadio == null) {
            response('gen', 'gendererror', 'No one selected');
            radiocheck = false;
        }else{
            radiocheck = true;
        }   
        return radiocheck;  
    }
   
    if((emp_name == true)&&(emp_ph_no == true)&&(emp_email == true)&&(emp_add == true)){ 
        $('#personaldetailsave').removeAttr('disabled');   
    }else{
        $('#personaldetailsave').prop('disabled', true);
    }
    
    //proffesionaldatasave
</script>
@stop

